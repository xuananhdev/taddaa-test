module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        alias: {
          '~': './src',
          '~components': './src/components',
          '~configs': './src/configs',
          '~store': './src/store',
          '~assets': './src/assets',
          '~pages': './src/pages',
          '~routes': './src/routes',
          '~services': './src/services',
          '~helper': './src/helper',
          '~hooks': './src/hooks',
          '~utils': './src/utils',
        },
        extensions: ['.ios.js', '.android.js', '.js', '.jsx', '.ts', '.tsx', '.json'],
      },
    ],
    'react-native-reanimated/plugin',
  ],
}
