import axios from '../configs/axios'
import queryString from 'query-string'

const auth = {
  login(payload: any) {
    return axios.post(`/connect/token`, queryString.stringify(payload), {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    })
  },

  refreshToken(payload: any) {
    return axios.post(`/connect/token`, queryString.stringify(payload), {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    })
  },

  forgotPassword(email: string) {
    return axios.post(`/api/employees/forgot-password?email=${email}`)
  },

  resetPassword(email: string, newPassword: string, code: string) {
    return axios.put(`/api/employees/reset-password`, {
      Email: email,
      Code: code,
      NewPassword: newPassword,
    })
  },
}

export default auth
