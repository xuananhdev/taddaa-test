import axios from '../configs/axios'
import queryString from 'query-string'

const userServices = {
  getProfile() {
    return axios.get(`/api/profiles/mine`)
  },
}

export default userServices
