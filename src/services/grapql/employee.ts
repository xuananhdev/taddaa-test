const QUERY_EMPPLOYEE = `
  query MyQuery($employeeId: String!) {
    eOffice_Employee(query: { contentItemId:$employeeId }) {
        items {
          contentItemId
          lastName
          firstName
          fullName
          email
          phoneNumber
          gender
          presentAddress
          contentItemId
          username
          employmentCategory
          startDate
          positionName
          status
          avatarUrl
          qualifications
          hourPerDay
          department {
            contentItems {
              contentItemId
              ... on Department {
                createdAt
                contentItemId
                name
              }
            }
          }
          position {
            contentItems {
              ... on JobPosition {
                contentItemId
                name
                positionLineManager {
                  contentItems {
                    ... on JobPosition {
                      contentItemId
                      name
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
`

export { QUERY_EMPPLOYEE }
