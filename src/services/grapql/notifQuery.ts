const QUERY_GET_NOTIFS = `
  query MyQuery($employeeId: String!, $skip: Int!, $first: Int!) {
    eOffice_Notification(query: { employeeId: $employeeId }, skip: $skip, first: $first) {
      items {
        contentItemId
        title
        description
        isRead
        relatedContentItemId
        relatedContentType
        rootRelatedContentItemId
        rootRelatedContentType
        createdAt
        employee {
          contentItemIds
        }
      }
    }
  }
`

export default QUERY_GET_NOTIFS
