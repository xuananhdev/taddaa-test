const QUERY_LIST_ABSENCE = `
  query MyQuery($employeeId: String!) {
        eOffice_Absence(query: { employeeId: $employeeId }) {
          items {
            contentItemId
            from
            to
            bag {
              contentItems {
                contentType
                ... on Review {
                  createdAt
                  createdBy
                  status
                  bag {
                    contentItems {
                      ... on ReviewApproverHistory {
                        isCurrent
                        employee {
                          contentItems {
                            ... on Employee {
                              annualSalary
                              avatarUrl
                              lastName
                              firstName
                              fullName
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
            deducted
            leaveType
            fromType
            toType
            isUnplaning
            unpaidLeaveOption
          }
      }
    }
`

export  {QUERY_LIST_ABSENCE}
