const QUERY_LIST_TASK = `
  query MyQuery($employeeId: String!) {
    eOffice_Task(query: { assigneeId: $employeeId, status: 0 }) {
      items {
        contentItemId
        name
        description
        dueDate
        myPriority
        status
        relatedContentItemId
        relatedContentType
        rootRelatedContentItemId
        rootRelatedContentType
        linkToDirect
        assignee {
          contentItems {
            ... on Employee {
              contentItemId
              avatarUrl
              firstName
              lastName
            }
          }
        }
      }
    }
  }
`

export { QUERY_LIST_TASK }
