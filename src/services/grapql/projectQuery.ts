const QUERY_LIST_PROJECT_ALLOCATION = `
    query MyQuery($employeeId: String!, $startDate: DateTime!, $endDate: DateTime!){
        eOffice_ProjectAllocation(
        query: {
            month_gte: $startDate
            month_lte: $endDate
            employeeId: $employeeId
        }
        orderBy: { month: ASC }
        ) {
        items {
            project {
            contentItems {
                ... on Project {
                contentItemId
                code
                name
                responsibility
                }
            }
            }
            percentage
            contentItemId
            month
            category
            responsibility
        }
        }
    }
`

export { QUERY_LIST_PROJECT_ALLOCATION }
