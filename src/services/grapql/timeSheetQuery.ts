const QUERY_LIST_TIME_SHEET = `
    query MyQuery($employeeId: String!, $date: DateTime!){
        eOffice_Timesheet(
          query: { employeeId: $employeeId, month: $date }
          orderBy: {}
        ) {
          totalCount
          items {
            contentItemId
            month
            from
            to
            category
            totalHours
            status
            employee {
              contentItems {
                ... on Employee {
                  contentItemId
                  fullName
                  firstName
                  lastName
                }
              }
            }
            owner
            bag {
              contentItems {
                ... on Review {
                  status
                  bag {
                    contentItems {
                      ... on ReviewApproverHistory {
                        isCurrent
                        employee {
                          contentItems {
                            ... on Employee {
                              fullName
                              contentItemId
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
`

export { QUERY_LIST_TIME_SHEET  }
