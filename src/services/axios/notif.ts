import axios from '../../configs/axios'
import queryString from 'query-string'
import QUERY_LIST_NOTIF from '../grapql/notifQuery'

const notifServices = {
  markAsRead(contentItemId: string) {
    return axios.post(`/api/notifications/${contentItemId}/mark-as-read`)
  },
  getListNotif(employeeId: string, skip: number, first: number) {
    return axios.post(`/api/graphql`, {
      query: QUERY_LIST_NOTIF,
      variables: { employeeId, skip, first },
    })
  },
}

export default notifServices
