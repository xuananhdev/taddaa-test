import axios from '../../configs/axios'
import queryString from 'query-string'
import { QUERY_EMPPLOYEE } from '../grapql/employee'

const employeeServices = {
  getEmployee(employeeId: string) {
    return axios.post(`/api/graphql`, {
      query: QUERY_EMPPLOYEE,
      variables: { employeeId },
    })
  },
}

export default employeeServices
