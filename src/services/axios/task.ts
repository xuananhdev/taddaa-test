import axios from '../../configs/axios'
import queryString from 'query-string'
import { QUERY_LIST_TASK } from '../grapql/taskQuery'

const taskServices = {
  setPriority(ContentItemId: string) {
    return axios.post(`/api/content-item`, {
      ContentItemId: ContentItemId,
      Task: {
        MyPriority: {
          Value: true,
        },
      },
    })
  },
  setMarkAsDone(ContentItemId: string) {
    return axios.post(`/api/content-item`, {
      ContentItemId: ContentItemId,
      Task: {
        Status: {
          Value: '2',
        },
      },
    })
  },

  getListTask(employeeId: string) {
    return axios.post(`/api/graphql`, {
      query: QUERY_LIST_TASK,
      variables: { employeeId },
    })
  },
}

export default taskServices
