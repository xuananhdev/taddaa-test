import axios from '../../configs/axios'
import queryString from 'query-string'
import { QUERY_LIST_PROJECT_ALLOCATION } from '../grapql/projectQuery'
import {QUERY_LIST_ABSENCE  } from '../grapql/absenceQuery'

const projectServices = {
  getProjectAllocation(employeeId: string, startDate: string, endDate: string) {
    return axios.post(`/api/graphql`, {
      query: QUERY_LIST_PROJECT_ALLOCATION,
      variables: { employeeId, startDate, endDate },
    })
  }
}

export default projectServices
