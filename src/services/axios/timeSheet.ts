import axios from '../../configs/axios'
import queryString from 'query-string'
import { QUERY_LIST_TIME_SHEET  } from '../grapql/timeSheetQuery'

const timeSheetServices = {
  getTimeSheets(employeeId: string, date: string){
    return axios.post(`/api/graphql`, {
      query: QUERY_LIST_TIME_SHEET,
      variables: { employeeId, date },
    })
  }
}

export default timeSheetServices
