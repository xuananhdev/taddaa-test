import axios from '../../configs/axios'
import queryString from 'query-string'
import {QUERY_LIST_ABSENCE  } from '../grapql/absenceQuery'

const absenceServices = {
  getAbsences(employeeId: string){
    return axios.post(`/api/graphql`, {
      query: QUERY_LIST_ABSENCE,
      variables: { employeeId },
    })
  }
}

export default absenceServices
