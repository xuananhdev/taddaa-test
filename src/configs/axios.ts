import axios, { AxiosInstance, AxiosRequestConfig } from 'axios'
import AsyncStorage from '@react-native-async-storage/async-storage'
import authServices from '../services/auth'
import { store } from '../store/index'
import { reSetState } from '~store/userSlice'
import configConstants from './constants'

const serverUrl = configConstants.serverUrl

async function getToken() {
  let token = await AsyncStorage.getItem('accessToken')
  if (!token) return ''
  return token
}

const config: AxiosRequestConfig = {
  baseURL: `${serverUrl}`,
  timeout: 60 * 1000, //ms
  // withCredentials: true,
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
    // Authorization: `Bearer ${getToken()}`,
    // "Access-Control-Allow-Credentials": true,
  },
}

const instance: AxiosInstance = axios.create(config)

//xử lý data trước khi lên server
instance.interceptors.request.use(
  async (config) => {
    if (!config?.headers || !config?.url) {
      throw new Error(`Expected 'config' and 'config.headers' or 'config.url' not to be undefined`)
    }

    // White list router don't check token
    if (
      config.url.indexOf('/connect/token') >= 0 ||
      config.url.indexOf('/api/employees/forgot-password') >= 0 ||
      config.url.indexOf('`/api/employees/reset-password') >= 0
    ) {
      return config
    }

    const token = await getToken()
    config.headers.Authorization = `Bearer ${token}`

    return config
  },
  (err) => {
    console.log('Error interceptors step 01:', err)
    return Promise.reject(err)
  },
)

// handle data from server response
instance.interceptors.response.use(
  async (response) => {
    let config = response.config
    if (!config) {
      config = {}
    }
    if (!config?.url) {
      config.url = ''
    }
    if (!config?.headers) {
      config.headers = {}
    }
    // White list router don't check token
    if (
      config.url.indexOf('/connect/token') >= 0 ||
      config.url.indexOf('/api/employees/forgot-password') >= 0 ||
      config.url.indexOf('`/api/employees/reset-password') >= 0
    ) {
      return response
    }

    return response
  },
  async (err) => {
    let config = err.config

    if (!config) {
      config = {}
    }

    if (!config?.url) {
      config.url = ''
    }

    if (!config?.headers) {
      config.headers = {}
    }
    const code = err.response.status

    if (code && code === 401) {
      try {
        const refreshToken = await AsyncStorage.getItem('refreshToken')
        if (!refreshToken) {
          return Promise.reject('Refresh token not found')
        }
        // step 1: get token from refresh token
        const res = await authServices.refreshToken({
          grant_type: configConstants.grant_type.refresh_token,
          client_id: configConstants.client_id,
          refresh_token: refreshToken, // get refresh token
        })

        const { access_token, refresh_token } = res.data

        if (access_token && refresh_token) {
          //step 2: set header with token
          config.headers.Authorization = `Bearer ${access_token}`
          // config.headers['X-Token'] = accessToken
          //step 3:
          await AsyncStorage.setItem('accessToken', access_token)
          await AsyncStorage.setItem('refreshToken', refresh_token)

          const failedResopnse = await axios.request(config)
          if (failedResopnse) {
            instance(config)
            return failedResopnse
          }
          // return instance(config)
        }
      } catch (error) {
        console.log('Error axios refresh token', error)
        if (code && code === 400) {
          await AsyncStorage.clear()
          store.dispatch(reSetState({}))
        }
        return Promise.reject(err)
      }

      return Promise.reject(err)
    }
    return Promise.reject(err)
  },
)

// /////////////////////////////////////////////////////////
// //set
// instance.setLocalAccessToken = async (accessToken) => {
//   await localStorage.setItem('accessToken', accessToken)
// }
// //get
// instance.getLocalAccessToken = async () => {
//   const accessToken = await localStorage.getItem('accessToken')
//   return accessToken ? accessToken : null
// }
export default instance
