const configConstants = {
  serverUrl: 'https://eoffice-api-sit.futurify.io',
  client_id: 'aav-eoffice-client',
  grant_type: {
    refresh_token: 'refresh_token',
    password: 'password',
  },
  scope: {
    offline_access: 'offline_access',
  },
}

export default configConstants
