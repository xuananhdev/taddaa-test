import React from 'react'
import { StyleSheet, View } from 'react-native'
import MainNavigator from './routes/routers'
import { SafeAreaView, StatusBar, useColorScheme } from 'react-native'
import { Colors } from 'react-native/Libraries/NewAppScreen'
// Redux
import { Provider as ReduxProvider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { persistor, store } from './store/index'
import { SafeAreaProvider, initialWindowMetrics } from 'react-native-safe-area-context'

const App = () => {
  const isDarkMode = useColorScheme() === 'dark'
  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
    flex: 1
  }
  return (
    <ReduxProvider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <SafeAreaProvider style={backgroundStyle} initialMetrics={initialWindowMetrics}>
          <StatusBar
            barStyle={isDarkMode ? 'light-content' : 'dark-content'}
            backgroundColor={'#fff'}
          />
          <View style={styles.container}>
            <MainNavigator />
          </View>
        </SafeAreaProvider>
      </PersistGate>
    </ReduxProvider>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
})

export default App
