import { StyleSheet, Text, View } from 'react-native'
import React from 'react'

import { createDrawerNavigator } from '@react-navigation/drawer'
import { DrawerContent } from './DrawerContent'
import SplashScreen from '../pages/SplashScreen/SplashScreen'

import Test from './TestTab'

const Drawer = createDrawerNavigator()

const DrawerNavigation = () => {
  return (
    <Drawer.Navigator
      screenOptions={{ headerShown: false }}
      drawerContent={(props) => <DrawerContent {...props} initialRouteName="Splash" />}
    >
      <Drawer.Screen name="Splash" component={SplashScreen} />
      <Drawer.Screen name="DashboardDR" component={Test} />
    </Drawer.Navigator>
  )
}

export default DrawerNavigation

const styles = StyleSheet.create({})
