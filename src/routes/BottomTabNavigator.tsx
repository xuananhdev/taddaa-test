import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { View, Text, Dimensions } from 'react-native'

import { BottomTabParamList } from './types'
import Dashboard from '../pages/Dashboard/Dashboard'
import Menu from '~pages/Menu/Menu'
import Profile from '~pages/Profile/Profile'
import colorConstant from '../constants/colors'
// import DashboardIcon from '../access/icons/dashboard.svg'
import MenuIcon from '../access/icons/MenuIcon'
import DashboardIcon from '../access/icons/HomeIcon'
import ProfileIcon from '../access/icons/ProfileIcon'
import DocumentIcon from '../access/icons/DocumentIcon'
import ExpenseIcon from '../access/icons/ExpenseIcon'

const SCREEN_WIDTH = Dimensions.get('window').width
const BottomTab = createBottomTabNavigator<BottomTabParamList>()

const BottomTabNavigator = () => {
  return (
    <BottomTab.Navigator
      initialRouteName="DashboardBT"
      screenOptions={{
        tabBarActiveTintColor: colorConstant.tabBarTintColor,
        tabBarInactiveTintColor: colorConstant.tabBarActiveTintColor,
        tabBarStyle: {
          position: 'absolute',
          height: 94,
          width: '100%',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#fffff',
          paddingHorizontal: 15,
        },
        headerShown: false,
        tabBarShowLabel: false,
        tabBarLabelStyle: {
          display: 'flex',
          flex: 1,
        },
        // tabBarIconStyle: {
        //   display: 'flex',
        //   flex: 1,
        // },
      }}
    >
      <BottomTab.Screen
        name="DashboardBT"
        component={Dashboard}
        options={{
          headerShown: false,
          tabBarIcon: ({ color, focused }) => (
            <>
              {focused ? (
                <View
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                    top: 5,
                    backgroundColor: colorConstant.RedBg,
                    paddingHorizontal: 19,
                    paddingVertical: 10,
                    borderRadius: 20,
                  }}
                >
                  <DashboardIcon color={color} size={18} />
                  <Text style={{ color: color, paddingLeft: 10, fontSize: 14 }}>Dashboard</Text>
                </View>
              ) : (
                <DashboardIcon color={color} />
              )}
            </>
          ),
          // tabBarLabel: 'Dashboard',
        }}
      />
      <BottomTab.Screen
        name="ProfileBT"
        component={Profile}
        options={{
          headerShown: false,
          tabBarIcon: ({ color, focused }) => (
            <>
              {focused ? (
                <View
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                    top: 5,
                    backgroundColor: colorConstant.RedBg,
                    paddingHorizontal: 19,
                    paddingVertical: 10,
                    borderRadius: 20,
                  }}
                >
                  <DocumentIcon color={color} />
                  <Text style={{ color: color, paddingLeft: 10 }}>TOR List</Text>
                </View>
              ) : (
                <DocumentIcon color={color} />
              )}
            </>
          ),

          // tabBarLabel: 'Profile',
        }}
      />
      <BottomTab.Screen
        name="MenuBT"
        component={Menu}
        options={{
          headerShown: false,
          tabBarIcon: ({ color, focused }) => (
            <>
              {focused ? (
                <View
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                    top: 5,
                    backgroundColor: colorConstant.RedBg,
                    paddingHorizontal: 19,
                    paddingVertical: 10,
                    borderRadius: 20,
                  }}
                >
                  <ExpenseIcon color={color} />
                  <Text style={{ color: color, paddingLeft: 10 }}>Expense List</Text>
                </View>
              ) : (
                <ExpenseIcon color={color} />
              )}
            </>
          ),
        }}
      />

      <BottomTab.Screen
        name="ExpenseBT"
        component={Menu}
        options={{
          headerShown: false,
          tabBarIcon: ({ color, focused }) => (
            <>
              {focused ? (
                <View
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                    top: 5,
                    backgroundColor: colorConstant.RedBg,
                    paddingHorizontal: 19,
                    paddingVertical: 10,
                    borderRadius: 20,
                  }}
                >
                  <ExpenseIcon color={color} />
                  <Text style={{ color: color, paddingLeft: 10 }}>Expense List</Text>
                </View>
              ) : (
                <ExpenseIcon color={color} />
              )}
            </>
          ),
        }}
      />
    </BottomTab.Navigator>
  )
}

export default React.memo(BottomTabNavigator)
