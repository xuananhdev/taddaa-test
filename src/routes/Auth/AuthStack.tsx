import * as React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack'

import LoginScreen from '~pages/Login/Login'
import LoginUserName from '~pages/Login/Components/LoginUserName/LoginUsername'
import LoginPassword from '~pages/Login/Components/LoginPassword/LoginPassword'
import VerifyCode from '~pages/VerifyCode/VerifyCode'
import ForgotPassword from '~pages/ForgotPassword/ForgotPassword'
import ChangePassword from '~pages/ChangePassword/ChangePassword'

import HeaderAuth from '~components/Header/HeaderAuth'

import { RootStackParamList } from '../types'

const Stack = createNativeStackNavigator<RootStackParamList>()

const AuthStackScreen = () => {
  return (
    <>
      <Stack.Screen name="Login" options={{ header: () => null }} component={LoginScreen} />
      <Stack.Screen
        name="LoginUserName"
        options={{
          header: (props) => <HeaderAuth {...props} />,
          animation: 'simple_push',
          // headerShown: false
        }}
        component={LoginUserName}
      />
      <Stack.Screen
        name="LoginPassword"
        options={{
          header: (props) => <HeaderAuth {...props} />,
          animation: 'simple_push',
        }}
        component={LoginPassword}
      />

      <Stack.Screen
        name="VerifyCode"
        options={{
          header: (props) => <HeaderAuth {...props} />,
          animation: 'simple_push',
        }}
        component={VerifyCode}
      />

      <Stack.Screen
        name="ForgotPassword"
        options={{
          header: (props) => <HeaderAuth {...props} />,
          animation: 'simple_push',
        }}
        component={ForgotPassword}
      />

      <Stack.Screen
        name="ChangePassword"
        options={{
          header: (props) => <HeaderAuth {...props} />,
          animation: 'simple_push',
        }}
        component={ChangePassword}
      />
    </>
  )
}

export default AuthStackScreen
