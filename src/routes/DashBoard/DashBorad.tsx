import * as React from 'react'
import { View, Text, Image } from 'react-native'

import { createNativeStackNavigator } from '@react-navigation/native-stack'

import Dashboard from '../../pages/Dashboard/Dashboard'

const AuthStack = createNativeStackNavigator()

const LogoTitle = ({ ...props }) => {
  return <Text style={{ color: '#F63042', fontWeight: 'bold', fontSize: 16 }}>act!onaid</Text>
}

function DashbardStackScreen() {
  return (
    <AuthStack.Navigator>
      <AuthStack.Screen name="Dashboard" component={Dashboard} />
    </AuthStack.Navigator>
  )
}

export default DashbardStackScreen
