import React from 'react'
import { Image, Text, TouchableOpacity, View, Platform } from 'react-native'
import {
  AnimatedTabBarNavigator,
  DotSize,
  TabElementDisplayOptions,
} from 'react-native-animated-nav-tab-bar'
import { BottomTabParamList } from './types'

import DashboardIcon from '../access/icons/HomeIcon'
import DocumentIcon from '../access/icons/DocumentIcon'
import ExpenseIcon from '../access/icons/ExpenseIcon'

import Dashboard from '../pages/Dashboard/Dashboard'
import Menu from '~pages/Menu/Menu'
import Profile from '~pages/Profile/CollapsibleList/Index'
import Expense from '~pages/Expense/Expense'

import colorConstant from '../constants/colors'

import ProfileDetail from '../pages/Profile/Detail'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { ProfileStackParamList } from './types'

import { useSelector } from 'react-redux'
import { appUserSelector } from '~store/userSlice/selector'

const Stack = createNativeStackNavigator<ProfileStackParamList>()

const Tabs = AnimatedTabBarNavigator<BottomTabParamList>()

// interface BTNavigation {
//   avatarUrl: string | null
//   lastName: string
// }

const TabProfileStack = (): JSX.Element => {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="Profile" component={Profile} />
      <Stack.Screen name="ProfileDetail" component={ProfileDetail} />
    </Stack.Navigator>
  )
}

const BottomTabNavigator = () => {
  const { lastName, avatarUrl } = useSelector(appUserSelector)
  return (
    <Tabs.Navigator
      initialRouteName="DashboardBT"
      tabBarOptions={{
        activeTintColor: colorConstant.tabBarTintColor,
        inactiveTintColor: colorConstant.tabBarActiveTintColor,
        activeBackgroundColor: colorConstant.RedBg,
        tabStyle: {
          // height: Platform.OS === 'ios' ? 90 : 94,
          width: '100%',
          backgroundColor: colorConstant.white,
          borderTopWidth: 1,
          borderTopColor: colorConstant.line,
          // position: 'absolute',
          // bottom: Platform.OS === 'ios' ? -30 : 0,
          paddingBottom: Platform.OS === 'ios' ? 16 : 20,
        },
      }}
      appearance={{
        shadow: true,
        floating: false,
        whenActiveShow: TabElementDisplayOptions.ICON_ONLY,
        dotSize: DotSize.MEDIUM,
        // topPadding: Platform.OS === 'ios' ? 15 : 15,
        // horizontalPadding: Platform.OS === 'ios' ? 10 : 25,
        bottomPadding: Platform.OS === 'ios' ? 16 : 20,
      }}
    >
      <Tabs.Screen
        name="DashboardBT"
        component={Dashboard}
        options={{
          tabBarIcon: ({ focused, color, size }: any) => (
            <>
              {focused ? (
                <View
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                    //   backgroundColor: colorConstant.RedBg,
                  }}
                >
                  <DashboardIcon color={color} size={18} />
                  <Text style={{ color: color, paddingLeft: 10, fontSize: 14 }}>Dashboard</Text>
                </View>
              ) : (
                <DashboardIcon color={color} />
              )}
            </>
          ),
        }}
      />
      <Tabs.Screen
        name="TORBT"
        component={Expense}
        options={{
          tabBarIcon: ({ focused, color, size }: any) => (
            <>
              {focused ? (
                <View
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: colorConstant.RedBg,
                  }}
                >
                  <DocumentIcon color={color} size={18} />
                  <Text style={{ color: color, paddingLeft: 10, fontSize: 14 }}>TOR List</Text>
                </View>
              ) : (
                <DocumentIcon color={color} size={18} />
              )}
            </>
          ),
        }}
      />
      <Tabs.Screen
        name="ExpenseBT"
        component={Expense}
        options={{
          tabBarIcon: ({ focused, color, size }: any) => (
            <>
              {focused ? (
                <View
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: colorConstant.RedBg,
                  }}
                >
                  <ExpenseIcon color={color} size={18} />
                  <Text style={{ color: color, paddingLeft: 10, fontSize: 14 }}>Expense List</Text>
                </View>
              ) : (
                <ExpenseIcon color={color} />
              )}
            </>
          ),
        }}
      />
      <Tabs.Screen
        name="ProfileBT"
        component={TabProfileStack}
        options={{
          tabBarIcon: ({ focused, color, size }: any) => (
            <>
              {focused ? (
                <View
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: colorConstant.RedBg,
                  }}
                >
                  {avatarUrl ? (
                    <>
                      <View
                        style={{
                          width: 30,
                          height: 30,
                          borderRadius: 15,
                          backgroundColor: colorConstant.red,
                          justifyContent: 'center',
                          alignItems: 'center',
                          borderWidth: 1,
                          borderColor: colorConstant.metaGray,
                          overflow: 'hidden',
                        }}
                      >
                        <Image
                          style={{
                            width: '100%',
                            height: '100%',
                            resizeMode: 'cover',
                          }}
                          source={{
                            uri: avatarUrl,
                          }}
                        />
                      </View>
                      <Text style={{ color: color, paddingLeft: 10, fontSize: 14 }}>
                        My Profile
                      </Text>
                    </>
                  ) : (
                    <>
                      <View
                        style={{
                          width: 30,
                          height: 30,
                          borderRadius: 15,
                          backgroundColor: colorConstant.red,
                          justifyContent: 'center',
                          alignItems: 'center',
                          borderWidth: 1,
                          borderColor: '#fff',
                        }}
                      >
                        <Text style={{ color: '#fff', fontSize: 14 }}>{lastName.slice(0, 1)}</Text>
                      </View>
                      <Text style={{ color: color, paddingLeft: 10, fontSize: 14 }}>
                        My Profile
                      </Text>
                    </>
                  )}
                </View>
              ) : (
                <>
                  {avatarUrl ? (
                    <>
                      <View
                        style={{
                          width: 30,
                          height: 30,
                          borderRadius: 15,
                          backgroundColor: colorConstant.red,
                          justifyContent: 'center',
                          alignItems: 'center',
                          borderWidth: 1,
                          borderColor: colorConstant.metaGray,
                          overflow: 'hidden',
                        }}
                      >
                        <Image
                          style={{ width: '100%', height: '100%', resizeMode: 'cover' }}
                          source={{
                            uri: avatarUrl,
                          }}
                        />
                      </View>

                      <Text style={{ color: color, paddingLeft: 10, fontSize: 14 }}>
                        My Profile
                      </Text>
                    </>
                  ) : (
                    <View
                      style={{
                        width: 30,
                        height: 30,
                        borderRadius: 15,
                        backgroundColor: colorConstant.red,
                        justifyContent: 'center',
                        alignItems: 'center',
                        borderWidth: 1,
                        borderColor: '#fff',
                      }}
                    >
                      <Text style={{ color: '#fff' }}>{lastName.slice(0, 1)}</Text>
                    </View>
                  )}
                </>
              )}
            </>
          ),
        }}
      />
    </Tabs.Navigator>
  )
}

export default React.memo(BottomTabNavigator)
