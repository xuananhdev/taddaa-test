import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import SplashScreen from '../pages/SplashScreen/SplashScreen'

// List stack
import AuthStackScreen from './Auth/AuthStack'
// redux
import { useSelector } from 'react-redux'
import { appUserSelector } from '~store/userSlice/selector'

import { RootStackParamList } from './types'

import DrawerNavigation from './Drawer'

const Stack = createNativeStackNavigator<RootStackParamList>()

const Routers = () => {
  const { token, avatarUrl, lastName } = useSelector(appUserSelector)

  console.log('avatarUrlavatarUrlavatarUrl', avatarUrl)
  // is Auth get from redux
  const isAuth = token ? true : false
  return (
    <NavigationContainer>
      {isAuth ? (
        <DrawerNavigation />
      ) : (
        <Stack.Navigator initialRouteName="Splash">
          <Stack.Screen name="Splash" options={{ header: () => null }} component={SplashScreen} />
          {AuthStackScreen()}
        </Stack.Navigator>
      )}
    </NavigationContainer>
  )
}

export default React.memo(Routers)

{
  /* <NavigationContainer>
<Stack.Navigator initialRouteName="Splash">
  <Stack.Screen name="Splash" options={{ header: () => null }} component={SplashScreen} />
  {isAuth ? (
    <Stack.Screen
      name="Dashboard"
      options={{
        headerShown: false,
        animation: 'simple_push',
      }}
      component={() => <Test avatarUrl={null} lastName={'ANH'} />}
    />
  ) : (
    AuthStackScreen()
  )}
</Stack.Navigator>
</NavigationContainer> */
}
