// Định nghĩa kiểu dữ liệu của navigation tại đây: https://reactnavigation.org/docs/typescript/#type-checking-the-navigator
import type {
  CompositeScreenProps,
  NavigatorScreenParams,
  RouteProp,
  ParamListBase,
} from '@react-navigation/native'

import type {
  NativeStackNavigationProp,
  NativeStackScreenProps,
} from '@react-navigation/native-stack'

import { createDrawerNavigator, DrawerNavigationProp } from '@react-navigation/drawer'

import type { BottomTabScreenProps } from '@react-navigation/bottom-tabs'

export type BottomTabParamList = {
  DashboardBT: undefined
  TORBT: undefined
  ExpenseBT: undefined
  ProfileBT: undefined
  MenuBT: undefined
}

export type RootStackParamList = {
  AuthModule: undefined
  Dashboard: NavigatorScreenParams<BottomTabParamList>
  Splash: undefined
  Login: undefined
  LoginUserName: undefined
  LoginPassword: { username: string }
  VerifyCode: { email: string }
  ForgotPassword: undefined
  ChangePassword: { email: string; code: string }
}

export type RootStackScreenProps<T extends keyof RootStackParamList> = NativeStackScreenProps<
  RootStackParamList,
  T
>

export type HomeTabScreenProps<T extends keyof BottomTabParamList> = CompositeScreenProps<
  BottomTabScreenProps<BottomTabParamList, T>,
  RootStackScreenProps<keyof RootStackParamList>
>
//PROFILE_TAB
export type ProfileStackParamList = {
  Profile: undefined
  ProfileDetail: {
    position: string
    phoneNumber: string
    email: string
    address: string
    username: string
    startDate: string
    department: string
    hourPerDay: number
  }
}

// Khi định nghĩa như thế này thì ở những nơi dùng hook useNavigation, useRoute thì ta không cần phải định nghĩa type
declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace ReactNavigation {
    // eslint-disable-next-line @typescript-eslint/no-empty-interface
    interface RootParamList extends RootStackParamList {}
  }
}

// Một cách khác khi sử dụng đối với useNavigation, useRoute

export type RootStackNavigationProps<T extends keyof RootStackParamList> =
  NativeStackNavigationProp<RootStackParamList, T>

export type RootStackRouteProps<T extends keyof RootStackParamList> = RouteProp<
  RootStackParamList,
  T
>
// TAB
//Profile
export type ProfileStackNavigationProps<T extends keyof ProfileStackParamList> =
  NativeStackNavigationProp<ProfileStackParamList, T>
