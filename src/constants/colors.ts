// export const tabBarTintColor = '#E3253C'
// export const tabBarActiveTintColor = '#5F5F5F'

const colorConstant = {
  tabBarTintColor: '#E3253C',
  tabBarActiveTintColor: '#5F5F5F',
  RedBg: '#FFF6F6',
  red: '#E3253C',
  metaGray: '#665C5D',
  bg: '#F6F6F6',
  dark: '#323232',
  black: '#000000',
  blue: '#4C75DF',
  blue500: '#5297FF',
  white: '#fff',
  green: '#67C365',
  line: '#D3D6D9',
  orange: '#F0723C',
}

export default colorConstant
