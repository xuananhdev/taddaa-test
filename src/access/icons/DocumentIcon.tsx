import React from 'react'
import Svg, { Path, Defs, G, Rect, ClipPath } from 'react-native-svg'
import colorConstant from '~/constants/colors'

interface Props {
  size?: number
  background?: string
  color?: string
}

/** icon home, outline */
const MenuOutlineIcon: React.FC<Props> = ({ size = 18, color = colorConstant.tabBarTintColor }) => {
  return (
    <Svg width={`${size}`} height={`${size}`} viewBox="0 0 20 24" fill="none">
      <Path
        d="M5.33317 16.6668H14.6665V19.0002H5.33317V16.6668ZM5.33317 12.0002H14.6665V14.3335H5.33317V12.0002ZM12.3332 0.333496H2.99984C1.7165 0.333496 0.666504 1.3835 0.666504 2.66683V21.3335C0.666504 22.6168 1.70484 23.6668 2.98817 23.6668H16.9998C18.2832 23.6668 19.3332 22.6168 19.3332 21.3335V7.3335L12.3332 0.333496ZM16.9998 21.3335H2.99984V2.66683H11.1665V8.50016H16.9998V21.3335Z"
        fill={`${color}`}
      />
    </Svg>
  )
}

export default MenuOutlineIcon
