import React from 'react'
import Svg, { Path, Defs, G, Rect, ClipPath } from 'react-native-svg'
import colorConstant from '~/constants/colors'

interface Props {
  size?: number
  background?: string
  color?: string
}

/** icon home, outline */
const MenuOutlineIcon: React.FC<Props> = ({ size = 18, color = colorConstant.tabBarTintColor }) => {
  return (
    <Svg width={`${size}`} height={`${size}`} viewBox="0 0 16 16" fill="none">
      <Path
        d="M0.333336 3.66659H3.66667V0.333252H0.333336V3.66659ZM5.33334 13.6666H8.66667V10.3333H5.33334V13.6666ZM0.333336 13.6666H3.66667V10.3333H0.333336V13.6666ZM0.333336 8.66658H3.66667V5.33325H0.333336V8.66658ZM5.33334 8.66658H8.66667V5.33325H5.33334V8.66658ZM10.3333 0.333252V3.66659H13.6667V0.333252H10.3333ZM5.33334 3.66659H8.66667V0.333252H5.33334V3.66659ZM10.3333 8.66658H13.6667V5.33325H10.3333V8.66658ZM10.3333 13.6666H13.6667V10.3333H10.3333V13.6666Z"
        fill={`${color}`}
      />
    </Svg>
  )
}

export default MenuOutlineIcon
