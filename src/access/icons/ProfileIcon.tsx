import React from 'react'
import Svg, { Path, Defs, G, Rect, ClipPath } from 'react-native-svg'
import colorConstant from '~/constants/colors'

interface Props {
  size?: number
  background?: string
  color?: string
}

/** icon home, outline */
const MenuOutlineIcon: React.FC<Props> = ({ size = 18, color = colorConstant.tabBarTintColor }) => {
  return (
    <Svg width={`${size}`} height={`${size}`} viewBox="0 0 16 16" fill="none">
      <Path
        d="M7.00001 7.83325C4.77501 7.83325 0.333344 8.94992 0.333344 11.1666V13.6666H13.6667V11.1666C13.6667 8.94992 9.22501 7.83325 7.00001 7.83325ZM12.0833 12.0833H1.91668V11.1666C1.91668 10.6333 4.52501 9.41658 7.00001 9.41658C9.47501 9.41658 12.0833 10.6333 12.0833 11.1666V12.0833ZM7.00001 6.99992C8.84168 6.99992 10.3333 5.50825 10.3333 3.66659C10.3333 1.82492 8.84168 0.333252 7.00001 0.333252C5.15834 0.333252 3.66668 1.82492 3.66668 3.66659C3.66668 5.50825 5.15834 6.99992 7.00001 6.99992ZM7.00001 1.91659C7.96668 1.91659 8.75001 2.69992 8.75001 3.66659C8.75001 4.63325 7.96668 5.41659 7.00001 5.41659C6.03334 5.41659 5.25001 4.63325 5.25001 3.66659C5.25001 2.69992 6.03334 1.91659 7.00001 1.91659Z"
        fill={`${color}`}
      />
    </Svg>
  )
}

export default MenuOutlineIcon
