import React from 'react'
import Svg, { Path, Defs, G, Rect, ClipPath } from 'react-native-svg'
import colorConstant from '~/constants/colors'

interface Props {
  size?: number
  background?: string
  color?: string
}

/** icon home, outline */
const HomeOutlineIcon: React.FC<Props> = ({ size = 18, color = colorConstant.tabBarTintColor }) => {
  return (
    <Svg width={`${size}`} height={`${size}`} viewBox="0 0 16 16" fill="none">
      <Path
        d="M0.5 8.83333H7.16667V0.5H0.5V8.83333ZM0.5 15.5H7.16667V10.5H0.5V15.5ZM8.83333 15.5H15.5V7.16667H8.83333V15.5ZM8.83333 0.5V5.5H15.5V0.5H8.83333Z"
        fill={`${color}`}
      />
    </Svg>
  )
}

export default HomeOutlineIcon
