import React from 'react'
import Svg, { Path, Defs, G, Rect, ClipPath } from 'react-native-svg'
import colorConstant from '~/constants/colors'

interface Props {
  size?: number
  background?: string
  color?: string
}

/** icon home, outline */
const MenuOutlineIcon: React.FC<Props> = ({ size = 18, color = colorConstant.tabBarTintColor }) => {
  return (
    <Svg width={`${size}`} height={`${size}`} viewBox="0 0 22 24" fill="none">
      <Path
        d="M19.75 2.08325L18 0.333252L16.25 2.08325L14.5 0.333252L12.75 2.08325L11 0.333252L9.25 2.08325L7.5 0.333252L5.75 2.08325L4 0.333252L2.25 2.08325L0.5 0.333252V23.6666L2.25 21.9166L4 23.6666L5.75 21.9166L7.5 23.6666L9.25 21.9166L11 23.6666L12.75 21.9166L14.5 23.6666L16.25 21.9166L18 23.6666L19.75 21.9166L21.5 23.6666V0.333252L19.75 2.08325ZM19.1667 20.2716H2.83333V3.72825H19.1667V20.2716ZM4 15.4999H18V17.8333H4V15.4999ZM4 10.8333H18V13.1666H4V10.8333ZM4 6.16658H18V8.49992H4V6.16658Z"
        fill={`${color}`}
      />
    </Svg>
  )
}

export default MenuOutlineIcon
