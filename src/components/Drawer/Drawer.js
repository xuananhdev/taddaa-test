import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import React, { useState, forwardRef, useImperativeHandle } from 'react'
import MenuDrawer from 'react-native-side-drawer'

const Drawer = forwardRef((props, ref) => {
  const [isOpen, setOpen] = useState(false)

  useImperativeHandle(ref, () => ({
    toggleOpen: () => toggleOpen(),
  }))

  const toggleOpen = () => {
    setOpen(!isOpen)
  }

  const drawerContent = () => {
    return (
      <TouchableOpacity onPress={toggleOpen} style={styles.animatedBox}>
        <Text>Close</Text>
      </TouchableOpacity>
    )
  }

  return (
    <>
      <MenuDrawer
        open={isOpen}
        position={'right'}
        drawerContent={drawerContent()}
        drawerPercentage={45}
        animationTime={250}
        overlay={true}
        opacity={0.4}
      />
    </>
  )
})

export default React.memo(Drawer)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 30,
    zIndex: 0,
  },
  animatedBox: {
    flex: 1,
    backgroundColor: '#38C8EC',
    padding: 10,
  },
  body: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F04812',
  },
})
