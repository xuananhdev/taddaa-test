import { View, StyleSheet, TouchableOpacity } from 'react-native'
import React, { useState } from 'react'

import LogoIcon from '../../access/icons/logo.svg'
import BackIcon from '../../access/icons/back.svg'

const HeaderAuth = (props: any) => {
  return (
    <View
      style={{
        height: 130,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 27,
      }}
    >
      <TouchableOpacity onPress={() => props.navigation.goBack()}>
        <BackIcon />
      </TouchableOpacity>

      <View style={{ marginLeft: 14 }}>
        <LogoIcon />
      </View>
    </View>
  )
}
export default React.memo(HeaderAuth)
