import { View, StyleSheet, Modal, TouchableOpacity, Text } from 'react-native'
import React, { useState, useEffect } from 'react'
import { Button } from '@ant-design/react-native'
import TadaaIcon from '../../access/icons/logoTadaa.svg'
import BellIcon from '../../access/icons/bell.svg'
import MenuIcon from '../../access/icons/menu.svg'
import Notif from '../../pages/Notification/Notification'
import colorConstant from '../../constants/colors'

import { appUserSelector } from '~store/userSlice/selector'
import { notifListSelector } from '~store/notifSlice/slector'

import { useDispatch, useSelector } from 'react-redux'
import notifServices from '~/services/axios/notif'
import { saveNotif } from '~store/notifSlice'

const Header = (props: any) => {
  const [isShowModal, setShowModal] = useState(false)
  const dispatch = useDispatch()
  const { employeeId } = useSelector(appUserSelector)
  const listNotifSelector = useSelector(notifListSelector)

  const [isReadedAll, setReadedAll] = useState(false)

  useEffect(() => {
    if (employeeId) {
      try {
        async function getListNotif() {
          const resListNotif = await notifServices.getListNotif(employeeId, 0, 20)
          const listNotification = resListNotif?.data?.data?.eOffice_Notification?.items
          dispatch(saveNotif({ listNotification }))
        }
        getListNotif()
      } catch (error) {
        console.log('getListNotif error', error)
      }
    }
  }, [employeeId])

  useEffect(() => {
    if (listNotifSelector?.length) {
      const checkReaded = listNotifSelector.find((item: any) => item.isRead === false)
      if (!checkReaded) {
        setReadedAll(true)
      } else {
        setReadedAll(false)
      }
    }
  }, [listNotifSelector?.length])

  return (
    <>
      <View style={styles.hederWraper}>
        <Button
          style={{ backgroundColor: 'transparent', borderColor: 'transparent', paddingTop:20 }}
          onPress={() => {
            setShowModal(true)
          }}
        >
          <View style={styles.notifIconWraper}>
            <BellIcon />
            {!isReadedAll ? <View style={styles.dotNotif} /> : null}
          </View>
        </Button>

        <View style={{paddingTop:20}}> 
          <TadaaIcon />
        </View>


        <TouchableOpacity
          onPress={() => {
            props.navigation.toggleDrawer()
          }}
          style={{paddingTop:20}}
        >
          <MenuIcon />
        </TouchableOpacity>
      </View>

      <Modal animationType="slide" transparent={false} visible={isShowModal}>
        <Notif setShowModal={setShowModal} />
      </Modal>
    </>
  )
}

export default React.memo(Header)

const styles = StyleSheet.create({
  hederWraper: {
    backgroundColor: '#FFFFFF',
    height: 100,
    width: ' 100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 24,
    borderBottomColor: colorConstant.line,
    borderBottomWidth: 1,
  },
  notifIconWraper: {
    position: 'relative',
  },

  dotNotif: {
    height: 10,
    width: 10,
    borderRadius: 5,
    position: 'absolute',
    right: -5,
    top: -4,
    backgroundColor: '#E3253C',
  },
})
