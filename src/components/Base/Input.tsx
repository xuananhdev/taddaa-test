import { TextInput, TextInputProps, StyleSheet, View, Text, TouchableOpacity } from 'react-native'
import React, { useState } from 'react'
import EyeSolid from '../../access/icons/eyeSolid.svg' // import SVG
import EyeSolidClose from '../../access/icons/eyeSolid.svg'
import EyeSolidIcon from '../../access/icons/eye_closed.svg'

type PropsType = TextInputProps & {
  //Or should I using TextInput instead of TextInputProps?
  LeftIcon?: React.ReactNode
  error?: boolean | false
  secureTextEntry?: boolean | false
}

const Input = (props: PropsType) => {
  // const [isFocused, setIsfocus] = useState(false)
  const [isShowPass, setShowPass] = useState(true)

  // const handleFocus = () => setIsfocus(true)

  // const handleBlur = () => setIsfocus(false)

  return (
    <View style={[styles.inputWraper, { borderColor: props.error ? 'red' : '#D3D6D9' }]}>
      {props.LeftIcon ? <View style={styles.leftIcon}>{props.LeftIcon}</View> : null}
      <TextInput
        // onFocus={handleFocus}
        // onBlur={handleBlur}
        {...props}
        style={[styles.input, props.style]}
        blurOnSubmit
        autoCorrect={false}
        secureTextEntry={props.secureTextEntry ? isShowPass : false}
        // keyboardType="default"
        //   maxLength={2}
      />
      {props.secureTextEntry ? (
        <TouchableOpacity
          onPress={() => {
            setShowPass((state) => !state)
          }}
          style={{ marginHorizontal: 8 }}
        >
          {isShowPass ? <EyeSolidIcon /> : <EyeSolidClose />}
        </TouchableOpacity>
      ) : null}
    </View>
  )
}

const styles = StyleSheet.create({
  leftIcon: {
    padding: 5,
    paddingLeft: 15,
  },
  inputWraper: {
    height: 50,
    borderRadius: 3,
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
    width: '100%',
    borderWidth: 1,
    borderColor: '#D3D6D9',
  },
  input: {
    flex: 1,
    marginVertical: 3,
    marginHorizontal:5
  },
})

export default React.memo(Input)
