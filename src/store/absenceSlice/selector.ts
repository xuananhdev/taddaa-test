import { RootState } from '../index'

export const listAbsenceSelector = (state: RootState) => state.absence.listNotification
