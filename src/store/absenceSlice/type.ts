import { string } from "yup/lib/locale"

 type Bag  = { 
  contentItems: any
}

export interface AbsenceType {
    contentItemId: string
    from:string
    to: string
    bag: Bag
    deducted: number
    leaveType: number
    fromType: number
    toType: number
    isUnplaning: boolean
    unpaidLeaveOption: number | null
  }
  