import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'
import type { AbsenceType } from './type'

//?:  co the dung hoac k dung, ! chac chan se cos , & dung chug type tranh lap lai code

// Define a type for the slice state
interface TaskState {
    listNotification: AbsenceType[]
  }
  
  const initialState: TaskState = {
        listNotification: [],
  }
  
  export const notifSlice = createSlice({
    name: 'notif',
    initialState,
    reducers: {
      saveNotif: (state, action: PayloadAction<TaskState>) => {
        state.listNotification = action.payload.listNotification
      },
      changeIsRead: (state, action) => {
        let findIndex = state.listNotification.findIndex(
          (todo) => todo.contentItemId === action.payload.contentItemId,
        )
  
        state.listNotification[findIndex] = action.payload
        state.listNotification = state.listNotification
      },
    },
  })
  
  export const { saveNotif, changeIsRead } = notifSlice.actions
  
  export default notifSlice.reducer
  