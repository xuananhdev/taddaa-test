export type NotificationType = {
  contentItemId: string
  title: string
  description: string
  isRead: boolean
  relatedContentItemId: boolean
  relatedContentType: string
  rootRelatedContentItemId: string
  rootRelatedContentType: string
  createdAt: string
  employee: {}
}
