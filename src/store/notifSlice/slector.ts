import { RootState } from '../index'

export const notifListSelector = (state: RootState) => state.notif.listNotification
