type Bag = {
  contentItems: any
}

export interface TimeSheetType {
  contentItemId: string
  month: string
  from: string
  to: string
  category: number
  totalHours: number
  status: number
  employee:{}
  owner: string
  bag: Bag
}
