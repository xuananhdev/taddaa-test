import { RootState } from '../index'

export const taskTodoSelector = (state: RootState) => state.task.needTodo
export const taskPrioritySelector = (state: RootState) => state.task.priority
