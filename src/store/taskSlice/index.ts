import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'
import type { TaskType } from './type'

//?:  co the dung hoac k dung, ! chac chan se cos , & dung chug type tranh lap lai code

// Define a type for the slice state
interface TaskState {
  needTodo: TaskType[]
  priority: TaskType[]
}

const initialState: TaskState = {
  needTodo: [],
  priority: [],
}

export const taskSlice = createSlice({
  name: 'task',
  initialState,
  reducers: {
    saveTask: (state, action: PayloadAction<TaskState>) => {
      state.needTodo = action.payload.needTodo
      state.priority = action.payload.priority
    },
    setPriority: (state, action) => {
      state.needTodo = state.needTodo.filter(
        (item) => item.contentItemId !== action.payload.contentItemId,
      )
      state.priority = [action.payload, ...state.priority]
    },
    setMarkAsDone: (state, action) => {
      const endPoint = action.payload === 'needToDo' ? state.needTodo : state.priority
      let findIndex = endPoint.findIndex(
        (todo) => todo.contentItemId === action.payload.contentItemId,
      )

      endPoint[findIndex] = action.payload

      if (action.payload === 'needToDo') {
        state.needTodo = endPoint
      } else {
        state.priority = endPoint
      }
    },
  },
})

export const { saveTask, setPriority, setMarkAsDone } = taskSlice.actions

export default taskSlice.reducer
