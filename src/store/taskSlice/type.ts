export type TaskType = {
  assignee: {}
  contentItemId: string | null
  description: string | null
  dueDate: string
  linkToDirect: string | null
  myPriority: boolean
  name: string
  relatedContentItemId: string
  relatedContentType: string
  rootRelatedContentItemId: string
  rootRelatedContentType: string
  status: number
  isDone?: boolean
  __typename: string
}
