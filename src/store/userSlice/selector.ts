import { RootState } from '../index'

export const appUserSelector = (state: RootState) => state.appUser
