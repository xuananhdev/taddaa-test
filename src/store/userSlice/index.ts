import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'

// Define a type for the slice state
interface AppUserState {
  token: string | null
  refresh_token: string | null
  avatarUrl: string | null
  employeeId: string | ''
  firstName: string | ''
  fullName: string | ''
  lastName: string | ''
  lineManager: string | ''
  lineManagerId: string | ''
  positionLineManager: string | ''
  Operations: string | ''
  positionLineManagerId: string | ''
  startDate: string | ''
  userId: string | ''
}

const initialState: AppUserState = {
  token: null,
  refresh_token: null,
  avatarUrl: null,
  employeeId: '',
  firstName: '',
  fullName: '',
  lastName: '',
  lineManager: '',
  lineManagerId: '',
  positionLineManager: '',
  Operations: '',
  positionLineManagerId: '',
  startDate: '',
  userId: '',
}

export const appUserSlice = createSlice({
  name: 'appUser',
  initialState,
  reducers: {
    saveInfoUser: (state, action: PayloadAction<AppUserState>) => {
      //   console.log({ payload: action.payload })
      state.token = action.payload.token
      state.refresh_token = action.payload.refresh_token
      state.avatarUrl = action.payload.avatarUrl
      state.employeeId = action.payload.employeeId
      state.firstName = action.payload.firstName
      state.fullName = action.payload.fullName
      state.lastName = action.payload.lastName
      state.lineManager = action.payload.lineManager
      state.lineManagerId = action.payload.lineManagerId
      state.positionLineManager = action.payload.positionLineManager
      state.Operations = action.payload.Operations
      state.positionLineManagerId = action.payload.positionLineManagerId
      state.startDate = action.payload.startDate
      state.userId = action.payload.userId
    },
    reSetState: (state, action) => {
      // state = initialState
      state.token = null
      state.refresh_token = null
      state.avatarUrl = null
      state.employeeId = ''
      state.firstName = ''
      state.fullName = ''
      state.lastName = ''
      state.lineManager = ''
      state.lineManagerId = ''
      state.positionLineManager = ''
      state.Operations = ''
      state.positionLineManagerId = ''
      state.startDate = ''
      state.userId = ''
    },
  },
})

export const { saveInfoUser, reSetState } = appUserSlice.actions

export default appUserSlice.reducer
