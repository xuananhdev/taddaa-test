import { combineReducers } from '@reduxjs/toolkit'

import appUserReducer from './userSlice/index'
import taskReducer from './taskSlice/index'
import notifReducer from './notifSlice/index'
import absenceReducer from './absenceSlice/index'

export const reducers = {
  appUser: appUserReducer,
  task: taskReducer,
  notif: notifReducer,
  absence: absenceReducer,
}

const rootReducer = combineReducers(reducers)

export default rootReducer
