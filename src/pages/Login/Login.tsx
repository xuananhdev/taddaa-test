import { View, Text, Image, Dimensions } from 'react-native'
import { StyleSheet } from 'react-native'
import { Button } from '@ant-design/react-native'
import React from 'react'

import type { RootStackParamList } from '~routes/types'
import type { NativeStackScreenProps } from '@react-navigation/native-stack'

type Props = NativeStackScreenProps<RootStackParamList, 'Login'>

const SCREEN_HEIGHT = Dimensions.get('window').height

const Login = ({ navigation }: Props) => {
  const toLogin = () => {
    navigation.navigate('LoginUserName')
  }

  return (
    <View style={styles.root}>
      <View style={styles.headImgWraper}>
        <Image
          source={require('../../access/images/hero.png')}
          style={styles.image}
          resizeMode="cover"
        />

        <Text style={styles.headerTile}>act!onaid</Text>

        <View style={styles.headerDescription}>
          <Text style={styles.headerDescriptionTitle}>
            We cause a revolution to fight for equality, men and women will have equal rights.
          </Text>

          <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
            <View style={{ height: 1, width: 24, backgroundColor: '#fff' }} />
            <Text style={styles.headerDescriptionTitle}> President Ho Chi Minh</Text>
          </View>
        </View>
      </View>
      <View style={styles.contentContainer}>
        <View style={styles.tileWraper}>
          <Text
            style={{
              ...styles.contentYear,
              color: 'red',
              fontWeight: 'bold',
              marginRight: 5,
            }}
          >
            /
          </Text>
          <Text style={styles.contentYear}>2018 - 2023</Text>
        </View>
        <Text style={styles.contentHeading}>ActionAid Vietnam Country Strategy Paper</Text>
        <Text style={styles.contentHeadingDes}>Advancing Human Rights for Social Justice</Text>
      </View>

      <View style={styles.footerContainer}>
        <Button
          type="warning"
          style={{ width: '100%', backgroundColor: '#E3253C' }}
          onPress={toLogin}
        >
          Login
        </Button>
        <Text style={styles.titleEnd}>© 2022 ActionAid Vietnam. All rights reserved.</Text>
      </View>
    </View>
  )
}

export default React.memo(Login)

const styles = StyleSheet.create({
  root: {
    display: 'flex',
    flex: 1,
    overflow: 'hidden',
  },
  headImgWraper: {
    flex: 1,
    // marginHorizontal: 10,
    position: 'relative',
    borderBottomLeftRadius: (SCREEN_HEIGHT * 0.25) / 2,
    borderBottomRightRadius: (SCREEN_HEIGHT * 0.25) / 2,
  },
  image: {
    flex: 1,
    height: undefined,
    width: undefined,
    borderBottomLeftRadius: (SCREEN_HEIGHT * 0.25) / 2,
    borderBottomRightRadius: (SCREEN_HEIGHT * 0.25) / 2,
    // aspectRatio: 1,
  },
  headerTile: {
    position: 'absolute',
    top: 42,
    left: 10,
    color: 'red',
    fontSize: 20,
    fontWeight: 'bold',
  },
  headerDescription: {
    position: 'absolute',
    top: '50%',
    left: 10,
  },
  headerDescriptionTitle: {
    color: '#fff',
    fontSize: 20,
    fontWeight: '400',
    lineHeight: 27,
  },
  contentContainer: {
    flex: 1,
    paddingHorizontal: 24,
    justifyContent: 'center',
    alignItems: 'flex-start',
    // backgroundColor: 'red',
  },

  tileWraper: {
    display: 'flex',
    flexDirection: 'row',
  },

  tileSpecial: {
    color: 'red',
    fontWeight: 'bold',
  },

  contentYear: {
    fontSize: 16,
    fontWeight: '800',
    lineHeight: 22,
  },
  contentHeading: {
    fontSize: 26,
    fontWeight: '700',
    lineHeight: 36,
    color: '#323232',
  },
  contentHeadingDes: {
    fontSize: 16,
    fontWeight: '400',
    lineHeight: 30,
    color: '#5F5F5F',
  },
  footerContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 24,
    marginBottom: 32,
  },

  titleEnd: {
    fontSize: 14,
    color: '#5F5F5F',
    marginTop: 16,
    lineHeight: 19,
  },
})