import {
  StyleSheet,
  Text,
  View,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Platform,
  Keyboard,
  StatusBar,
} from 'react-native'
import React, { useState } from 'react'

import { Button, ActivityIndicator } from '@ant-design/react-native'
import { Input } from '~components/index'
import { Formik, Form, ErrorMessage } from 'formik'
import AuthVaidateScheme from '../../validate'
import AuthServices from '~/services/auth'
import { useDispatch } from 'react-redux'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { saveInfoUser } from '~store/userSlice'
import userServices from '~/services/user'
import Dialog from 'react-native-dialog'
import configConstants from '~/configs/constants'

import type { RootStackParamList } from '~routes/types'
import type { NativeStackScreenProps } from '@react-navigation/native-stack'

import PasswordIcon from '../../../../access/icons/password.svg'
import CircleSolidIcon from '../../../../access/icons/CircleSolidIcon'

import { useHeaderHeight } from '@react-navigation/elements'

type Props = NativeStackScreenProps<RootStackParamList, 'LoginPassword'>

const LoginPassword = ({ navigation, route }: Props) => {
  const headerHeight = useHeaderHeight()
  const statusBarHeight = StatusBar.currentHeight || 0

  const KEYBOARD_VERTICAL_OFFSET = headerHeight + statusBarHeight + 30

  const dispatch = useDispatch()
  const [isLogin, setIsLogin] = useState<Boolean | false>()
  const [visible, setVisible] = useState(false)

  const handleGoHome = async (password: string) => {
    try {
      let username = route.params.username
      setIsLogin(true) // demo indicator
      const loginData = await AuthServices.login({
        // username: 'sysadmin',
        // password: 'demo@123',
        // username: 'trang.phamthu',
        // password: '123456',
        username: username,
        password: password,
        grant_type: configConstants.grant_type.password,
        client_id: configConstants.client_id,
        scope: configConstants.scope.offline_access, // get refresh token
      })

      if (loginData?.data?.access_token) {
        await AsyncStorage.setItem('accessToken', loginData.data.access_token)
        await AsyncStorage.setItem('refreshToken', loginData.data.refresh_token)
        const userProfile = await userServices.getProfile()

        if (userProfile?.data) {
          const {
            avatarUrl,
            employeeId,
            firstName,
            fullName,
            lastName,
            lineManager,
            lineManagerId,
            positionLineManager,
            Operations,
            positionLineManagerId,
            startDate,
            userId,
          } = userProfile?.data
          dispatch(
            saveInfoUser({
              avatarUrl,
              employeeId,
              firstName,
              fullName,
              lastName,
              lineManager,
              lineManagerId,
              positionLineManager,
              Operations,
              positionLineManagerId,
              startDate,
              userId,
              token: loginData?.data?.access_token,
              refresh_token: loginData?.data?.refresh_token,
            }),
          )
        }
      }

      setIsLogin(false) // demo indicator
    } catch (error) {
      setVisible(true)
      console.log('login error', error)
    }
  }
  return (
    <>
      <Formik
        validationSchema={AuthVaidateScheme}
        initialValues={{ password: '' }}
        onSubmit={(values) => {}}
      >
        {({ handleChange, handleBlur, handleSubmit, values, errors, isValid }) => {
          return (
            <KeyboardAvoidingView
              behavior={Platform.OS === 'ios' ? 'padding' : undefined}
              style={{ display: 'flex', flex: 1 }}
              keyboardVerticalOffset={KEYBOARD_VERTICAL_OFFSET}
              // enabled={Platform.OS === 'ios' ? true : false}
            >
              <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <View style={styles.authContainer}>
                  <Text style={styles.heading}>Login</Text>
                  <Text style={styles.description}>Enter your password to log in</Text>

                  <Text style={styles.label}>
                    Password<Text style={styles.specicalText}>*</Text>
                  </Text>
                  <Input
                    LeftIcon={<PasswordIcon />}
                    placeholder={'Enter your password'}
                    onChangeText={handleChange('password')}
                    value={values.password}
                    error={errors.password ? true : false}
                    secureTextEntry={true}
                  />
                  <Text
                    style={styles.forgotPass}
                    onPress={() => navigation.replace('ForgotPassword')}
                  >
                    Forgot your password?
                  </Text>

                  <View style={styles.buttonWraper}>
                    <Button
                      disabled={errors.password || !values.password ? true : false}
                      type="warning"
                      style={{
                        width: '100%',
                        backgroundColor: '#E3253C',
                      }}
                      onPress={() => handleGoHome(values.password)}
                    >
                      {isLogin ? <ActivityIndicator color="#fff" /> : 'Login'}
                    </Button>
                  </View>
                </View>
              </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
          )
        }}
      </Formik>

      <Dialog.Container
        visible={visible}
        contentStyle={{
          display: 'flex',
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <View style={styles.dialogContainer}>
          <View
            style={{
              display: 'flex',
              flexDirection: 'row',
              paddingBottom: 25,
              alignItems: 'center',
            }}
          >
            <CircleSolidIcon />
            <Text style={{ marginLeft: 6 }}>
              The specifield usernam/password couple is invailid.
            </Text>
          </View>

          <View style={{ width: '100%' }}>
            <Dialog.Button
              label="Ok"
              // Reset naviagtion remove all stack
              onPress={() => {
                setVisible(false)
                setIsLogin(false)
              }}
              style={{
                width: 300,
              }}
            />
          </View>
        </View>
      </Dialog.Container>
    </>
  )
}

export default React.memo(LoginPassword)

const styles = StyleSheet.create({
  //diaglog
  dialogContainer: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10
  },
  //
  specicalText: { color: '#E3253C' },
  authContainer: {
    display: 'flex',
    flex: 1,
    paddingHorizontal: 24,
  },
  heading: {
    color: '#323232',
    fontWeight: '700',
    fontSize: 26,
    lineHeight: 36,
  },
  description: {
    fontSize: 16,
    lineHeight: 22,
    color: '#5F5F5F',
  },
  label: {
    fontSize: 16,
    fontWeight: '600',
    paddingTop: 32,
    paddingBottom: 12,
  },
  buttonWraper: {
    position: 'absolute',
    bottom: 0,
    left: 24,
    right: 24,
    marginBottom: 30,
  },
  forgotPass: {
    color: '#E3253C',
    paddingTop: 16,
    fontSize: 16,
    fontWeight: '600',
  },
})
