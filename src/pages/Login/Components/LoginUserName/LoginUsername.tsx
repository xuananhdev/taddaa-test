import {
  StyleSheet,
  Text,
  View,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Platform,
  Keyboard,
  StatusBar,
} from 'react-native'
import React from 'react'
import { Button } from '@ant-design/react-native'
import { Input } from '~components/index'
import { Formik, Form, ErrorMessage } from 'formik'
import AuthVaidateScheme from '../../validate'
import UserNameIcon from '../../../../access/icons/userIcon.svg'

import type { RootStackParamList } from '~routes/types'
import type { NativeStackScreenProps } from '@react-navigation/native-stack'
import { useHeaderHeight } from '@react-navigation/elements'

type Props = NativeStackScreenProps<RootStackParamList, 'LoginUserName'>

const LoginUsername = ({ navigation }: Props) => {
  const headerHeight = useHeaderHeight()
  const statusBarHeight = StatusBar.currentHeight || 0
  const KEYBOARD_VERTICAL_OFFSET = headerHeight + statusBarHeight + 30

  return (
    <Formik
      validationSchema={AuthVaidateScheme}
      initialValues={{ username: '' }}
      onSubmit={(values) => console.log('onSubmit::', values)}
    >
      {({ handleChange, handleBlur, handleSubmit, values, errors, isValid }) => (
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : undefined}
          style={{ display: 'flex', flex: 1 }}
          keyboardVerticalOffset={KEYBOARD_VERTICAL_OFFSET}
        >
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <View style={styles.authContainer}>
              <Text style={styles.heading}>Login</Text>
              <Text style={styles.description}>Enter your registered email to log in</Text>

              {/* {errors.username && <Text style={{ fontSize: 10, color: 'red' }}>{errors.username}</Text>} */}
              <Text style={styles.label}>
                Username<Text style={styles.specicalText}>*</Text>
              </Text>
              <Input
                LeftIcon={<UserNameIcon />}
                placeholder={'Enter your username'}
                onChangeText={handleChange('username')}
                value={values.username}
                error={errors.username ? true : false}
                autoFocus={false}
              />

              <View style={styles.buttonWraper}>
                <Button
                  disabled={errors.username || !values.username ? true : false}
                  type="warning"
                  style={{
                    width: '100%',
                    backgroundColor: '#E3253C',
                  }}
                  onPress={() => {
                    navigation.navigate('LoginPassword', { username: values.username })
                  }}
                >
                  Continue
                </Button>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
      )}
    </Formik>
  )
}

export default React.memo(LoginUsername)

const styles = StyleSheet.create({
  specicalText: { color: '#E3253C' },
  authContainer: {
    display: 'flex',
    flex: 1,
    paddingHorizontal: 24,
  },
  heading: {
    color: '#323232',
    fontWeight: '700',
    fontSize: 26,
    lineHeight: 36,
  },
  description: {
    fontSize: 16,
    lineHeight: 22,
    color: '#5F5F5F',
  },
  label: {
    fontSize: 16,
    fontWeight: '600',
    paddingTop: 32,
    paddingBottom: 12,
  },
  buttonWraper: {
    position: 'absolute',
    bottom: 0,
    left: 24,
    right: 24,
    marginBottom: 30,
  },
  //Test
  container: {
    flex: 1,
  },
  inner: {
    padding: 24,
    flex: 1,
    justifyContent: 'space-around',
  },
  header: {
    fontSize: 36,
    marginBottom: 48,
  },
  textInput: {
    height: 40,
    borderColor: '#000000',
    borderBottomWidth: 1,
    marginBottom: 36,
  },
  btnContainer: {
    backgroundColor: 'white',
    marginTop: 12,
  },
})
