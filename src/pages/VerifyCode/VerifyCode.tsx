import {
  StyleSheet,
  Text,
  View,
  Alert,
  TouchableOpacity,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Platform,
  Keyboard,
  StatusBar,
} from 'react-native'
import React, { useState } from 'react'
import { Button, ActivityIndicator } from '@ant-design/react-native'
import { Input } from '~components/index'

import verifyCodeSchema from './validate'
import { Formik, Form, ErrorMessage } from 'formik'
import authService from '~services/auth'
import { sleep } from '~utils/time'
import { useNavigation } from '@react-navigation/native'
import type { RootStackParamList } from '~routes/types'
import type { NativeStackScreenProps } from '@react-navigation/native-stack'

import { useHeaderHeight } from '@react-navigation/elements'

type Props = NativeStackScreenProps<RootStackParamList, 'VerifyCode'>

const VerifyCode = ({ route }: Props) => {
  const navigation = useNavigation()
  const [isReSentCode, setReSentCode] = useState(false)
  const email = route.params.email

  const headerHeight = useHeaderHeight()
  const statusBarHeight = StatusBar.currentHeight || 0
  const KEYBOARD_VERTICAL_OFFSET = headerHeight + statusBarHeight + 30

  const _resendCode = async () => {
    try {
      setReSentCode(true)
      const resForgot = await authService.forgotPassword(email)
      if (resForgot.status == 204) {
        await sleep(2000)
        setReSentCode(false)
        Alert.alert('Resend code done!!')
      }
    } catch (error) {
      await sleep(2000)
      Alert.alert('Resend code error!!')
      setReSentCode(false)
      console.log('resend Code error', error)
    }
  }

  const _changePassWord = (code: string) => {
    navigation.navigate('ChangePassword', { email, code })
  }

  return (
    <Formik
      validationSchema={verifyCodeSchema}
      initialValues={{ code: '' }}
      onSubmit={(values) => console.log('onSubmit::', values)}
    >
      {({ handleChange, handleBlur, handleSubmit, values, errors, isValid }) => (
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : undefined}
          style={{ display: 'flex', flex: 1 }}
          keyboardVerticalOffset={KEYBOARD_VERTICAL_OFFSET}
          // enabled={Platform.OS === 'ios' ? true : false}
        >
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <View style={styles.authContainer}>
              <Text style={styles.heading}>Reset password</Text>
              {/* Dynamic with email */}
              <Text
                style={styles.description}
              >{`A verification code has been sent to ${email}`}</Text>

              <Text style={styles.label}>Verification Code</Text>
              <Input
                placeholder={'Enter your code'}
                keyboardType="number-pad"
                maxLength={6}
                onChangeText={handleChange('code')}
                value={values.code}
                error={errors.code ? true : false}
                autoFocus={true}
              />

              <TouchableOpacity onPress={() => _resendCode()}>
                <Text style={styles.forgotPass}>
                  {isReSentCode ? <ActivityIndicator color="#E3253C" /> : 'Resend code'}
                </Text>
              </TouchableOpacity>

              <View style={styles.buttonWraper}>
                <Button
                  disabled={!values.code || errors.code ? true : false}
                  type="warning"
                  style={{
                    width: '100%',
                    backgroundColor: '#E3253C',
                  }}
                  onPress={() => {
                    _changePassWord(values.code)
                  }}
                >
                  Submit
                </Button>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
      )}
    </Formik>
  )
}

export default React.memo(VerifyCode)

const styles = StyleSheet.create({
  specicalText: { color: '#E3253C' },
  authContainer: {
    display: 'flex',
    flex: 1,
    paddingHorizontal: 24,
  },
  heading: {
    color: '#323232',
    fontWeight: '700',
    fontSize: 26,
    lineHeight: 36,
  },
  description: {
    fontSize: 16,
    lineHeight: 22,
    color: '#5F5F5F',
  },
  label: {
    fontSize: 16,
    fontWeight: '600',
    paddingTop: 32,
    paddingBottom: 12,
  },
  buttonWraper: {
    position: 'absolute',
    bottom: 0,
    left: 24,
    right: 24,
    marginBottom: 30,
  },
  forgotPass: {
    color: '#E3253C',
    paddingTop: 16,
    fontSize: 16,
    fontWeight: '600',
  },
})
