// App.js
import * as yup from 'yup'

const loginValidationSchema = yup.object().shape({
  code: yup
    .string()
    .min(6, ({ min }) => `Code must be at least ${min} characters`)
    .required('Code is required'),
})

export default loginValidationSchema
