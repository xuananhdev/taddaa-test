// import {
//   StyleSheet,
//   Animated,
//   View,
//   Image,
//   TouchableOpacity,
//   ScrollView,
//   Dimensions,
//   Platform,
// } from 'react-native'
// import React, { useRef, useState, useEffect } from 'react'
// import Header from '~components/Header/Header'
// import colorConstant from '../../constants/colors'
// import Typography from '~components/Base/Typography'

// import { Button, ActivityIndicator } from '@ant-design/react-native'
// import PhoneIcon from '../../access/icons/phoneProfile.svg'
// import EmailIcon from '../../access/icons/emailprofile.svg'

// import { useSafeAreaInsets } from 'react-native-safe-area-context'

// import Tabs from './components/Tabs/Tabs'

// import { useSelector } from 'react-redux'
// import { appUserSelector } from '~store/userSlice/selector'

// const SCREEN_HEIGHT = Dimensions.get('screen').height

// import type { ProfileStackNavigationProps } from '~routes/types'
// import { useNavigation } from '@react-navigation/native'

// import employeeServices from '~services/axios/employee'

// // const  HEADER_HEIGHT= 200

// const AnimatedHeader = ({
//   navigation,
//   isLoading,
//   animatedValue,
//   HEADER_HEIGHT,
//   avatarUrl,
//   lastName,
//   fullName,
//   position,
//   phoneNumber,
//   email,
//   address,
//   username,
//   startDate,
//   department,
//   hourPerDay,
// }: any) => {
//   const insets = useSafeAreaInsets()

//   // console.log("insets.top", insets.top);

//   const headerHeight = animatedValue.interpolate({
//     inputRange: [0, HEADER_HEIGHT + insets.top],
//     outputRange: [HEADER_HEIGHT + insets.top, insets.top + 40],
//     extrapolate: 'clamp',
//   })

// return (
//   <Animated.View
//     style={{
//       // position: 'absolute',
//       // top: 0,
//       // left: 0,
//       // right: 0,
//       // zIndex: 10,
//       height: headerHeight,
//     }}
//   >
//     <Typography
//       fontSize={22}
//       fontWeight="600"
//       color={colorConstant.dark}
//       paddingVertical={30}
//       paddingLeft={24}
//     >
//       My Profile
//     </Typography>

//     {isLoading ? (
//       <>
//         <View
//           style={{ display: 'flex', flex: 1, justifyContent: 'center', alignItems: 'center' }}
//         >
//           <ActivityIndicator text="Fetching profile..." color={'red'} size="large" />
//         </View>
//       </>
//     ) : (
//       <>
//         <View style={styles.detailContainer}>
//           <View style={styles.detailWraper}>
//             <View style={styles.profileImageWraper}>
//               <View style={styles.ImageWraper}>
//                 {avatarUrl ? (
//                   <Image
//                     style={styles.profileImage}
//                     source={{
//                       uri: avatarUrl,
//                     }}
//                     resizeMode="contain"
//                   />
//                 ) : (
//                   <View
//                     style={{
//                       ...styles.profileImage,
//                       backgroundColor: 'red',
//                       display: 'flex',
//                       justifyContent: 'center',
//                       alignItems: 'center',
//                     }}
//                   >
//                     <Typography fontSize={30} fontWeight={'600'} color="#fff">
//                       {lastName.slice(0, 1)}
//                     </Typography>
//                   </View>
//                 )}
//               </View>
//             </View>

//             <View style={{ display: 'flex', flex: 1, justifyContent: 'center' }}>
//               <Typography
//                 color={colorConstant.black}
//                 fontSize={18}
//                 lineHeight={19}
//                 fontWeight={'600'}
//               >
//                 {fullName}
//               </Typography>
//               <Typography color={colorConstant.black} paddingVertical={8}>
//                 {position}
//               </Typography>
//             </View>
//           </View>

//           <View style={{ display: 'flex', flexDirection: 'row' }}>
//             <PhoneIcon />
//             <Typography paddingLeft={15} color={colorConstant.black} fontSize={14}>
//               {phoneNumber}
//             </Typography>
//           </View>

//           <View style={{ display: 'flex', flexDirection: 'row', paddingTop: 10 }}>
//             <EmailIcon />
//             <Typography paddingLeft={15} color={colorConstant.black} fontSize={14}>
//               {email}
//             </Typography>
//           </View>

//           <View style={styles.buttonWraper}>
//             <Button
//               // type="warning"
//               style={{
//                 width: '45%',
//                 backgroundColor: '#fff',
//                 borderColor: colorConstant.blue,
//               }}
//               onPress={() => {
//                 navigation.navigate('ProfileDetail', {
//                   position,
//                   phoneNumber,
//                   email,
//                   address,
//                   username,
//                   startDate,
//                   department,
//                   hourPerDay,
//                 })
//               }}
//             >
//               <Typography color={colorConstant.blue} fontSize={14} fontWeight={'600'}>
//                 Details
//               </Typography>
//             </Button>

//             <Button
//               // type="warning"
//               style={{
//                 width: '45%',
//                 backgroundColor: '#fff',
//                 borderColor: colorConstant.blue,
//               }}
//               onPress={() => {}}
//             >
//               <Typography color={colorConstant.blue} fontSize={14} fontWeight={'600'}>
//                 Edit
//               </Typography>
//             </Button>
//           </View>
//         </View>
//       </>
//     )}
//   </Animated.View>
//   )
// }

// const Profile = () => {
//   const navigation = useNavigation<ProfileStackNavigationProps<'Profile'>>()
//   const { avatarUrl, lastName, fullName, employeeId } = useSelector(appUserSelector)

//   const [isLoading, setLoading] = useState(false)

//   const offset = useRef(new Animated.Value(0)).current
//   const [userProfileInfo, setUserProfileInfo] = useState<any>()

//   useEffect(() => {
//     if (employeeId) {
//       async function fetchEmployees() {
//         try {
//           setLoading(true)
//           const res = await employeeServices.getEmployee(employeeId)
//           setUserProfileInfo(res.data.data.eOffice_Employee.items[0])
//           setLoading(false)
//         } catch (error) {
//           console.log('fetchEmployees error: ', error)
//           setLoading(false)
//         }
//       }

//       fetchEmployees()
//     }
//   }, [employeeId])

//   const tabBarHeight = Platform.OS === 'ios' ? 140 : 135
//   return (
//     <View style={styles.profileContainer}>
//       <Header navigation={navigation} />

//       <ScrollView
//         style={{ display: 'flex', height: '100%' }}
//         // contentContainerStyle={{flexGrow:1}}
//         nestedScrollEnabled={true}
//         scrollEventThrottle={16}
//         onScroll={Animated.event([{ nativeEvent: { contentOffset: { y: offset } } }], {
//           useNativeDriver: false,
//         })}
//       >
//         <AnimatedHeader
//           navigation={navigation}
//           isLoading={isLoading}
//           animatedValue={offset}
//           HEADER_HEIGHT={SCREEN_HEIGHT / 2}
//           avatarUrl={avatarUrl}
//           lastName={lastName}
//           fullName={fullName}
//           position={userProfileInfo?.positionName || ''}
//           phoneNumber={userProfileInfo?.phoneNumber || ''}
//           email={userProfileInfo?.email || ''}
//           address={userProfileInfo?.presentAddress || ''}
//           username={userProfileInfo?.username || ''}
//           startDate={userProfileInfo?.startDate || ''}
//           department={userProfileInfo?.department?.contentItems?.[0]?.name || ''}
//           hourPerDay={userProfileInfo?.hourPerDay || ''}
//         />

//         <View style={{ height: SCREEN_HEIGHT - 107 - tabBarHeight }}>
//           <Tabs />
//         </View>
//       </ScrollView>
//     </View>
//   )
// }

// export default React.memo(Profile)

// const styles = StyleSheet.create({
// profileContainer: {
//   display: 'flex',
//   flex: 1,
//   flexDirection: 'column',
//   backgroundColor: colorConstant.bg,
// },
// detailContainer: {
//   display: 'flex',
//   flex: 1,
//   backgroundColor: '#fff',
//   paddingHorizontal: 25,
//   paddingBottom: 28,
// },

// detailWraper: {
//   display: 'flex',
//   flexDirection: 'row',
// },
// profileImageWraper: {
//   display: 'flex',
//   flex: 1,
//   flexDirection: 'row',
//   // position: 'relative',
// },
// ImageWraper: {
//   // position: 'absolute',
//   top: -20,
//   left: 30,
//   width: 111,
//   height: 111,
//   borderWidth: 11,
//   borderColor: '#fff',
//   borderRadius: 50,
//   display: 'flex',
//   justifyContent: 'center',
//   alignItems: 'center',
// },
// profileImage: {
//   width: 100,
//   height: 100,
//   borderRadius: 50,
// },
// buttonWraper: {
//   display: 'flex',
//   flexDirection: 'row',
//   justifyContent: 'space-between',
//   alignItems: 'center',
//   paddingTop: 27,
// },
// })

import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import {
  CollapsibleContainer,
  CollapsibleFlatList,
  CollapsibleScrollView,
  CollapsibleHeaderContainer,
  StickyView,
  withCollapsibleContext,
} from '@r0b0t3d/react-native-collapsible'

import Index from './CollapsibleList/Index'

const MyComponent = () => {
  return (
    <>
      <Index />
    </>
  )
}

export default MyComponent
