import { StyleSheet, Text, View, useWindowDimensions } from 'react-native'
import React, { useEffect, useState } from 'react'
import { TabView, SceneMap, TabBar } from 'react-native-tab-view'
import Absence from '../../CollapsibleList/components/Absence'
import Projects from '../../CollapsibleList/components/Projects'
import TimeSheet from '../../CollapsibleList/components/TimeSheet'
type TaskProps = {
  myPriority: boolean
}

const Tabs = () => {
  const layout = useWindowDimensions()
  const [index, setIndex] = React.useState(0)

  const [routes] = React.useState([
    { key: 'first', title: 'Absence' },
    { key: 'second', title: 'Projects' },
    { key: 'third', title: 'TimeSheet' },
  ])

  const renderTabBar = (props: any) => (
    <TabBar
      {...props}
      scrollEnabled={false}
      indicatorStyle={{ backgroundColor: '#E3253C' }}
      style={{
        backgroundColor: 'transparent',
        shadowOffset: { height: 0, width: 0 },
        shadowColor: 'transparent',
        shadowOpacity: 0,
        elevation: 0,
      }}
      pressColor={'transparent'}
      pressOpacity={undefined}
      // labelStyle={{ color: '#5F5F5F' }}
      renderLabel={({ route, focused, color }) => (
        <View
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Text style={{ color: focused ? '#323232' : '#5F5F5F', margin: 8 }}>{route.title}</Text>
        </View>
      )}
    />
  )

  const renderScene = SceneMap({
    first: () => <Absence />,
    second: () => <Projects />,
    third: () => <TimeSheet />,
  })

  return (
    <TabView
      navigationState={{ index, routes }}
      renderScene={renderScene}
      onIndexChange={setIndex}
      renderTabBar={(props) =>
        renderTabBar({
          ...props,
        })
      }
      initialLayout={{ width: layout.width }}
      style={{
        marginTop: 16,
        backgroundColor: 'red',
        height: 600,
      }}
    />
  )
}

export default Tabs

const styles = StyleSheet.create({})
