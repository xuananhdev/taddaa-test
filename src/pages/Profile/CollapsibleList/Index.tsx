import React, { useRef, useState, useEffect } from 'react'
import { Image, StyleSheet, Text, ScrollView, TouchableOpacity, View } from 'react-native'
import {
  CollapsibleContainer,
  CollapsibleHeaderContainer,
  CollapsibleHeaderText,
  CollapsibleView,
  StickyView,
  withCollapsibleContext,
} from '@r0b0t3d/react-native-collapsible'
import Top100Tab from './components/Top100Tab'
import AlbumsTab from './components/AlbumsTab'
import { useSafeAreaInsets } from 'react-native-safe-area-context'
import Header from '~components/Header/Header'
import colorConstant from '../../../constants/colors'
import Typography from '~components/Base/Typography'

import { Button, ActivityIndicator } from '@ant-design/react-native'
import PhoneIcon from '../../../access/icons/phoneProfile.svg'
import EmailIcon from '../../../access/icons/emailprofile.svg'

import { useSelector } from 'react-redux'
import { appUserSelector } from '~store/userSlice/selector'

import type { ProfileStackNavigationProps } from '~routes/types'
import { useNavigation } from '@react-navigation/native'

import employeeServices from '~services/axios/employee'
// ALl tabs
import Absence from './components/Absence'
import Projects from './components/Projects'
import TimeSheet from './components/TimeSheet'

import HeaderAbsence from './components/HeaderAbsence'
import HeaderProject from './components/HeaderProject'
import HeaderTimeSheet from './components/HeaderTimeSheet'

import moment from 'moment'

function CollapsibleList() {
  const [currentTab, setCurrentTab] = useState(0)
  const { top } = useSafeAreaInsets()
  //
  const navigation = useNavigation<ProfileStackNavigationProps<'Profile'>>()
  const { avatarUrl, lastName, fullName, employeeId } = useSelector(appUserSelector)

  const [isLoading, setLoading] = useState(false)

  const [userProfileInfo, setUserProfileInfo] = useState<any>()

  const [startDateProject, setStartDateProject] = useState(`${moment().format('YYYY-MM')}-01`)
  const [endDateProject, setEndDateProject] = useState(`${moment().format('YYYY-MM')}-31`)

  const [startDateTimeSheet, setStartDateTimeSheet] = useState(`${moment().format('YYYY-MM')}-01`)

  useEffect(() => {
    if (employeeId) {
      async function fetchEmployees() {
        try {
          setLoading(true)
          const res = await employeeServices.getEmployee(employeeId)
          setUserProfileInfo(res.data.data.eOffice_Employee.items[0])
          setLoading(false)
        } catch (error) {
          console.log('fetchEmployees error: ', error)
          setLoading(false)
        }
      }

      fetchEmployees()
    }
  }, [employeeId])

  // function renderHeder(props: any) {
  //   return <CollapsibleHeaderText title="Collapsible view" {...props} />
  // }

  return (
    <View style={styles.container}>
      <Header navigation={navigation} />
      <CollapsibleContainer style={styles.contentContainer}>
        <CollapsibleHeaderContainer>
          <View
            pointerEvents="none"
            style={{
              ...styles.infoContainer,
              height: 300,
              backgroundColor: colorConstant.bg,
            }}
          >
            <Typography
              fontSize={22}
              fontWeight="600"
              color={colorConstant.dark}
              paddingVertical={30}
              paddingLeft={24}
            >
              My Profile
            </Typography>

            {isLoading ? (
              <>
                <View
                  style={{
                    display: 'flex',
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                >
                  <Image
                    style={{ width: 100, height: 100 }}
                    source={require('../../../access/images/tadaa.gif')}
                    resizeMode="contain"
                  />
                </View>
              </>
            ) : (
              <>
                <View style={styles.detailContainer}>
                  <View style={styles.detailWraper}>
                    <View style={styles.profileImageWraper}>
                      <View style={styles.ImageWraper}>
                        {avatarUrl ? (
                          <Image
                            style={styles.profileImage}
                            source={{
                              uri: avatarUrl,
                            }}
                            resizeMode="contain"
                          />
                        ) : (
                          <View
                            style={{
                              ...styles.profileImage,
                              backgroundColor: 'red',
                              display: 'flex',
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}
                          >
                            <Typography fontSize={30} fontWeight={'600'} color="#fff">
                              {lastName.slice(0, 1)}
                            </Typography>
                          </View>
                        )}
                      </View>
                    </View>

                    <View style={{ display: 'flex', flex: 1, justifyContent: 'center' }}>
                      <Typography
                        color={colorConstant.black}
                        fontSize={18}
                        lineHeight={19}
                        fontWeight={'600'}
                      >
                        {fullName}
                      </Typography>
                      <Typography color={colorConstant.black} paddingVertical={8}>
                        {userProfileInfo?.positionName || ''}
                      </Typography>
                    </View>
                  </View>

                  <View style={{ display: 'flex', flexDirection: 'row' }}>
                    <PhoneIcon />
                    <Typography paddingLeft={15} color={colorConstant.black} fontSize={14}>
                      {userProfileInfo?.phoneNumber || ''}
                    </Typography>
                  </View>

                  <View style={{ display: 'flex', flexDirection: 'row', paddingTop: 10 }}>
                    <EmailIcon />
                    <Typography paddingLeft={15} color={colorConstant.black} fontSize={14}>
                      {userProfileInfo?.email || ''}
                    </Typography>
                  </View>
                </View>
              </>
            )}

            {/*  */}
          </View>

          {/* List Button */}
          <View style={styles.buttonWraper} pointerEvents="box-none">
            <Button
              // type="warning"
              style={{
                width: '45%',
                backgroundColor: '#fff',
                borderColor: colorConstant.blue,
              }}
              onPress={() => {
                navigation.navigate('ProfileDetail', {
                  position: userProfileInfo?.positionName || '',
                  phoneNumber: userProfileInfo?.phoneNumber || '',
                  email: userProfileInfo?.email || '',
                  address: userProfileInfo?.presentAddress || '',
                  username: userProfileInfo?.username || '',
                  startDate: userProfileInfo?.startDate || '',
                  department: userProfileInfo?.department?.contentItems?.[0]?.name || '',
                  hourPerDay: userProfileInfo?.hourPerDay || '',
                })
              }}
            >
              <Typography color={colorConstant.blue} fontSize={14} fontWeight={'600'}>
                Details
              </Typography>
            </Button>

            <Button
              // type="warning"
              style={{
                width: '45%',
                backgroundColor: '#fff',
                borderColor: colorConstant.blue,
              }}
              onPress={() => {}}
            >
              <Typography color={colorConstant.blue} fontSize={14} fontWeight={'600'}>
                Edit
              </Typography>
            </Button>
          </View>

          {/* TAB */}
          <StickyView style={{  backgroundColor: colorConstant.bg, marginTop: 30 }}>
            <View style={styles.tabsContainer} pointerEvents="auto">
              <TouchableOpacity
                style={[styles.tab, currentTab === 0 ? styles.tabSelected : {}]}
                onPress={() => setCurrentTab(0)}
              >
                <Text style={[styles.tabTitle, currentTab === 0 ? styles.tabTitleSelected : {}]}>
                  Absence
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.tab, currentTab === 1 ? styles.tabSelected : {}]}
                onPress={() => setCurrentTab(1)}
              >
                <Text style={[styles.tabTitle, currentTab === 1 ? styles.tabTitleSelected : {}]}>
                  Project allocation
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.tab, currentTab === 2 ? styles.tabSelected : {}]}
                onPress={() => setCurrentTab(2)}
              >
                <Text style={[styles.tabTitle, currentTab === 2 ? styles.tabTitleSelected : {}]}>
                  Timesheet
                </Text>
              </TouchableOpacity>
            </View>
            {currentTab === 0 ? (
              <View>
                <HeaderAbsence />
              </View>
            ) : currentTab === 1 ? (
              <View>
                <HeaderProject
                  setStartDateProject={setStartDateProject}
                  setEndDateProject={setEndDateProject}
                  startDateProject={startDateProject}
                  endDateProject={endDateProject}
                />
              </View>
            ) : (
              <View>
                <HeaderTimeSheet
                  setStartDateTimeSheet={setStartDateTimeSheet}
                  startDateTimeSheet={startDateTimeSheet}
                />
              </View>
            )}
            {/* <Tabs /> */}
          </StickyView>
        </CollapsibleHeaderContainer>
        {currentTab === 0 ? (
          <Absence />
        ) : currentTab === 1 ? (
          <Projects startDateProject={startDateProject} endDateProject={endDateProject} />
        ) : (
          <TimeSheet startDateTimeSheet={startDateTimeSheet} />
        )}
      </CollapsibleContainer>
    </View>
  )
}

export default withCollapsibleContext(CollapsibleList)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colorConstant.bg,
  },
  contentContainer: {
    flex: 1,
    backgroundColor: colorConstant.bg,
  },
  backgroundTop: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: '100%',
    backgroundColor: '#713D7C',
  },
  infoContainer: {
    // height: HEADER_HEIGHT + insets.top,
    // paddingTop: 60,
    // alignItems: 'center',
    // justifyContent: 'center',
  },
  // profileImage: {
  //   width: 80,
  //   height: 80,
  //   borderRadius: 40,
  //   borderWidth: 4,
  //   borderColor: 'white',
  //   shadowColor: '#000',
  //   shadowOffset: { width: 0, height: 1 },
  //   shadowOpacity: 0.8,
  //   shadowRadius: 1,
  // },
  name: {
    fontSize: 17,
    fontWeight: 'bold',
    marginTop: 10,
  },
  searchBox: {
    marginHorizontal: 20,
    backgroundColor: 'cyan',
    borderRadius: 20,
    paddingVertical: 10,
    paddingHorizontal: 10,
    marginBottom: 10,
  },
  tabsContainer: {
    flexDirection: 'row',
    // marginHorizontal: 20,
    marginTop: 30,
    marginBottom: 10,
    backgroundColor: '#fff',
    paddingVertical: 8,
  },
  tab: {
    flex: 1,
    alignItems: 'center',
    paddingVertical: 10,
    borderBottomWidth: 2,
    borderColor: 'transparent',
  },
  tabSelected: {
    borderColor: '#E3253C',
  },
  tabTitle: {
    fontWeight: 'bold',
    color: '#ACAEB0',
  },
  tabTitleSelected: {
    color: colorConstant.dark,
    fontWeight: '600',
  },
  collapsibleView: {
    height: 500,
    backgroundColor: 'yellow',
    justifyContent: 'flex-end',
  },

  //
  profileContainer: {
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    backgroundColor: colorConstant.bg,
  },
  detailContainer: {
    display: 'flex',
    flex: 1,
    backgroundColor: '#fff',
    paddingHorizontal: 25,
    paddingBottom: 28,
  },

  detailWraper: {
    display: 'flex',
    flexDirection: 'row',
  },
  profileImageWraper: {
    display: 'flex',
    flex: 1,
    flexDirection: 'row',
    // position: 'relative',
  },
  ImageWraper: {
    // position: 'absolute',
    top: -20,
    left: 30,
    width: 111,
    height: 111,
    borderWidth: 11,
    borderColor: '#fff',
    borderRadius: 50,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  profileImage: {
    width: 100,
    height: 100,
    borderRadius: 50,
  },
  buttonWraper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 24,
  },

  //sticky
})
