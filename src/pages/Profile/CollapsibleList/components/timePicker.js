import React, { useState } from 'react'
import { View, StyleSheet } from 'react-native'
import DatePicker from 'react-native-modern-datepicker'
import Modal from 'react-native-modal'
import moment from 'moment'

const MonthYearPicker = ({
  isShowModal,
  setShowModal,
  currentSelect = 'start',
  setStartDate,
  setEndDate,
  startDate,
  endDate,
}) => {
  //   const [isShowModal, setShowModal] = useState(false)

  return (
    <Modal isVisible={isShowModal} onBackdropPress={() => setShowModal()}>
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <DatePicker
          current={moment(currentSelect === 'start' ? startDate : endDate).format('YYYY/MM')}
          // selected="2022/2"
          mode="monthYear"
          selectorStartingYear={2000}
          onMonthYearChange={(selectedDate) => {
            if (currentSelect === 'start') {
              const startDay = moment(selectedDate, 'YYYY-MM-DD h:m').startOf('month').format('D')
              const timeJoin = `${selectedDate} ${startDay}`
              setStartDate(moment(timeJoin, 'YYYY-MM-DD h:m').format('YYYY-MM-DD'))
              setShowModal(false)
            } else {
              const lastDay = moment(selectedDate, 'YYYY-MM-DD h:m').endOf('month').format('D')
              const timeJoin = `${selectedDate} ${lastDay}`
              setEndDate(moment(timeJoin, 'YYYY-MM-DD h:m').format('YYYY-MM-DD'))
              setShowModal(false)
            }
          }}
        />
      </View>
    </Modal>
  )
}

export default React.memo(MonthYearPicker)  
