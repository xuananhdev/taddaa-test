import { StyleSheet, View, Image } from 'react-native'
import React, { useEffect, useState } from 'react'
import colorConstant from '../../../../constants/colors'
import Typography from '~components/Base/Typography'

import { Button } from '@ant-design/react-native'

import { useSelector } from 'react-redux'
import { appUserSelector } from '~store/userSlice/selector'
import moment from 'moment'

import projectServices from '~/services/axios/project'

import { CollapsibleScrollView } from '@r0b0t3d/react-native-collapsible'

const enumCategory = ['AAV', 'AFV']

const Projects = ({ startDateProject, endDateProject }: any) => {
  const [listProject, setListProject] = useState([])
  const [isLoading, setLoading] = useState(false)

  const { employeeId } = useSelector(appUserSelector)

  useEffect(() => {
    async function getProjectAllocation() {
      try {
        setLoading(true)
        const res = await projectServices.getProjectAllocation(
          employeeId,
          startDateProject,
          endDateProject,
        )

        setListProject(res.data.data.eOffice_ProjectAllocation.items)
        setLoading(false)
      } catch (error) {
        console.log('getProjectAllocation error', error)
        setLoading(false)
      }
    }

    if (startDateProject && endDateProject && employeeId) {
      getProjectAllocation()
    }
  }, [startDateProject, endDateProject, employeeId])

  return (
    <>
      {/* List item map */}
      {isLoading ? (
        <View style={{ display: 'flex', flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Image
            style={{ width: 100, height: 100 }}
            source={require('../../../../access/images/tadaa.gif')}
            resizeMode="contain"
          />
        </View>
      ) : (
        <CollapsibleScrollView>
          {listProject.map((item: any, index) => (
            <View
              style={{
                backgroundColor: colorConstant.white,
                marginTop: 18,
                paddingVertical: 30,
                paddingHorizontal: 24,
              }}
              key={index.toString()}
            >
              <Typography paddingTop={5} color={colorConstant.dark} fontWeight={'600'}>
                Project
              </Typography>
              <Typography
                paddingBottom={24}
                color={colorConstant.dark}
                fontSize={16}
                lineHeight={26}
              >
                {item.project.contentItems[0].name}
              </Typography>

              <View style={{ display: 'flex', flexDirection: 'row' }}>
                <View style={{ flex: 2 }}>
                  <Typography color={colorConstant.dark} fontWeight={'600'}>
                    Month
                  </Typography>
                  <Typography paddingVertical={8} color={colorConstant.dark} fontWeight={'600'}>
                    Ogranization
                  </Typography>
                  <Typography paddingVertical={8} color={colorConstant.dark} fontWeight={'600'}>
                    Percentage
                  </Typography>
                  <Typography color={colorConstant.dark} fontWeight={'600'}>
                    Responsibility
                  </Typography>
                </View>

                <View style={{ flex: 3 }}>
                  <Typography color={colorConstant.dark}>
                    {moment(item.month).format('MMMM YY')}
                  </Typography>
                  <Typography paddingVertical={8} color={colorConstant.dark}>
                    {enumCategory[item.category]}
                  </Typography>
                  <Typography paddingVertical={8} color={colorConstant.dark}>
                    {item.percentage}
                  </Typography>

                  <Typography color={colorConstant.dark}>{item.responsibility}</Typography>
                </View>
              </View>

              <View
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  paddingTop: 32,
                }}
              >
                <Button
                  // type="warning"
                  style={{
                    width: '45%',
                    backgroundColor: '#fff',
                    borderColor: colorConstant.blue,
                  }}
                  onPress={() => {}}
                >
                  <Typography color={colorConstant.blue} fontSize={14} fontWeight={'600'}>
                    Edit
                  </Typography>
                </Button>
                <Button
                  // type="warning"
                  style={{
                    width: '45%',
                    backgroundColor: colorConstant.white,
                    borderColor: colorConstant.red,
                  }}
                  onPress={() => {}}
                >
                  <Typography color={colorConstant.red} fontSize={14} fontWeight={'600'}>
                    Remove
                  </Typography>
                </Button>
              </View>
            </View>
          ))}
        </CollapsibleScrollView>
      )}
    </>
  )
}

export default React.memo(Projects)

const styles = StyleSheet.create({
  absenceContainer: {
    display: 'flex',
    flex: 1,
    backgroundColor: colorConstant.bg,
  },

  addWraper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    paddingHorizontal: 24,
  },
  leftAddWraper: {
    display: 'flex',
    flexDirection: 'row',

    alignItems: 'center',
  },

  rightAddWraper: {
    backgroundColor: colorConstant.red,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '30%',
    height: 42,
    borderRadius: 5,
  },
})
