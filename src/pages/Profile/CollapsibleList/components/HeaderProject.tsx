import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import React, { useState } from 'react'
import colorConstant from '../../../../constants/colors'
import Typography from '~components/Base/Typography'

import CongIcon from '../../../../access/icons/cong.svg'
import ArounfRightIcon from '../../../../access/icons/aroundRight.svg'
import CalendarIcon from '../../../../access/icons/calendar1.svg'

import MonthPicker from './timePicker'
import moment from 'moment'

const HeaderProject = ({
  setStartDateProject,
  setEndDateProject,
  startDateProject,
  endDateProject,
}: any) => {
  const [isShowModal, setShowModal] = useState(false)
  const [startDate, setStartDate] = useState(`${moment().format('YYYY-MM')}-01`)
  const [endDate, setEndDate] = useState(`${moment().format('YYYY-MM')}-31`)
  const [currentSelect, setCurrentSelect] = useState('start')
  return (
    <>
      <View style={styles.addWraper}>
        <View
          style={{
            display: 'flex',
            flexDirection: 'row',
            marginVertical: 30,
            paddingHorizontal: 8,
            alignItems: 'center',
            justifyContent: 'space-between',
            borderWidth: 1,
            borderColor: colorConstant.line,
            borderRadius: 5,
            height: 42,
          }}
        >
          <TouchableOpacity
            style={styles.leftAddWraper}
            onPress={() => {
              setCurrentSelect('start')
              setShowModal(!isShowModal)
            }}
          >
            <Typography>{moment(startDateProject).format('MMM YYYY')}</Typography>
          </TouchableOpacity>

          <View style={{ paddingHorizontal: 5 }}>
            <ArounfRightIcon />
          </View>

          <TouchableOpacity
            style={styles.leftAddWraper}
            onPress={() => {
              setCurrentSelect('end')
              setShowModal(!isShowModal)
            }}
          >
            <Typography>{moment(endDateProject).format('MMM YYYY')}</Typography>
          </TouchableOpacity>

          <View style={{ paddingLeft: 10 }}>
            <CalendarIcon />
          </View>
        </View>

        <TouchableOpacity style={styles.rightAddWraper}>
          <View style={{}}>
            <CongIcon />
          </View>

          <Typography marginLeft={10} color={'#fff'}>
            Add new
          </Typography>
        </TouchableOpacity>
      </View>
      <MonthPicker
        isShowModal={isShowModal}
        setShowModal={setShowModal}
        currentSelect={currentSelect}
        setStartDate={setStartDateProject}
        setEndDate={setEndDateProject}
        startDate={startDateProject}
        endDate={endDateProject}
      />
    </>
  )
}

export default React.memo(HeaderProject)

const styles = StyleSheet.create({
  addWraper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    paddingHorizontal: 24,
  },
  leftAddWraper: {
    display: 'flex',
    flexDirection: 'row',
    paddingVertical: 5,
    alignItems: 'center',
  },

  rightAddWraper: {
    backgroundColor: colorConstant.red,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '40%',
    height: 42,
    borderRadius: 5,
  },
})
