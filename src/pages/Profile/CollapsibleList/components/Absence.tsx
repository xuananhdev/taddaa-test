import { StyleSheet, View, TouchableOpacity, Image } from 'react-native'
import React, { useEffect, useState } from 'react'
import colorConstant from '../../../../constants/colors'
import Typography from '~components/Base/Typography'
import { Button } from '@ant-design/react-native'
import absenceServices from '~services/axios/absence'
import { useSelector } from 'react-redux'
import { appUserSelector } from '~store/userSlice/selector'
import moment from 'moment'
import { AbsenceType } from '~store/absenceSlice/type'
import { CollapsibleScrollView } from '@r0b0t3d/react-native-collapsible'

const LeaveType = [
  'AnnualLeave',
  'WeddingLeave',
  'CompassionateLeave',
  'LeaveWithoutPay',
  'ExtraTimeOfMaternityLeave',
  'MaternityLeave',
  'PaternityLeave',
  'OneMoreChild',
  'AdoptionLeave',
  'PrenatalCheckup',
  'LeaveInLieu',
  'SickLeave',
  'ChildSickLeave',
  'PublicHoliday',
  'AdvancedAnnualLeave',
]

const ReviewStatusType = ['Pending', 'Inprogress', 'Approved', 'Rejected']

export interface Iitem {
  item: AbsenceType
}

const Absence = () => {
  const [listAbsence, setListAbsence] = useState([])
  const [isLoading, setLoading] = useState(false)

  const { employeeId } = useSelector(appUserSelector)

  useEffect(() => {
    async function getAbsence() {
      try {
        setLoading(true)
        const res = await absenceServices.getAbsences(employeeId)
        if (res.data?.data?.eOffice_Absence?.items?.length) {
          setListAbsence(res.data.data.eOffice_Absence.items)
        }
        setLoading(false)
      } catch (error) {
        setLoading(false)
        console.log('get Absence error', error)
      }
    }
    getAbsence()
  }, [])

  const checkApprove = (bag: any): any => {
    let approveType = ''
    for (let i = 0; i < bag.contentItems.length; i++) {
      if (bag.contentItems[i].status == 2) {
        for (let j = 0; j < bag.contentItems[i].bag.contentItems.length; j++) {
          if (bag.contentItems[i].bag.contentItems[j].isCurrent) {
            approveType = bag.contentItems[i].bag.contentItems[j].employee.contentItems[0].fullName
          } else {
            continue
          }
        }
      } else {
        continue
      }
    }

    return approveType
  }

  const checkStatus = (isUnPlanning: boolean, status: number) => {
    if (isUnPlanning) {
      return `Unplanned  - ${ReviewStatusType[status]}`
    }
    return `Planned - ${ReviewStatusType[status]}`
  }

  return (
    <>
      {/*  */}
      {isLoading ? (
        <View style={{ display: 'flex', flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Image
            style={{ width: 100, height: 100 }}
            source={require('../../../../access/images/tadaa.gif')}
            resizeMode="contain"
          />
        </View>
      ) : (
        <CollapsibleScrollView
        // contentContainerStyle={{ flexGrow: 1 }}
        // nestedScrollEnabled={true}
        // showsVerticalScrollIndicator={false}
        >
          {listAbsence.map((item: any, index) => (
            <View
              style={{
                backgroundColor: colorConstant.white,
                marginTop: 18,
                paddingVertical: 30,
                paddingHorizontal: 24,
              }}
              key={index.toString()}
            >
              <TouchableOpacity
                style={{
                  height: 34,
                  width: '50%',
                  backgroundColor: item.isUnplaning ? colorConstant.blue500 : colorConstant.green,
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 5,
                }}
              >
                <Typography color={colorConstant.white}>
                  {checkStatus(item.isUnplaning, item.bag.contentItems[0].status)}
                </Typography>
              </TouchableOpacity>

              <Typography paddingTop={32} color={colorConstant.dark} fontWeight={'600'}>
                Date
              </Typography>
              <Typography
                paddingBottom={24}
                color={colorConstant.dark}
                fontSize={18}
                lineHeight={26}
              >
                {`${item.from ? moment(item.from).format('DD/MM/YYYY') : ''} - ${
                  item.to ? moment(item.to).format('DD/MM/YYYY') : ''
                }`}
              </Typography>

              <View style={{ display: 'flex', flexDirection: 'row' }}>
                <View style={{ flex: 2 }}>
                  <Typography color={colorConstant.dark} fontWeight={'600'}>
                    Type
                  </Typography>
                  <Typography paddingVertical={8} color={colorConstant.dark} fontWeight={'600'}>
                    Deducted
                  </Typography>
                  <Typography color={colorConstant.dark} fontWeight={'600'}>
                    Approved by
                  </Typography>
                </View>

                <View style={{ flex: 3 }}>
                  <Typography color={colorConstant.dark}>{LeaveType[item.leaveType]}</Typography>
                  <Typography paddingVertical={8} color={colorConstant.dark}>
                    {item.deducted}
                  </Typography>
                  <Typography color={colorConstant.dark}>{checkApprove(item.bag)}</Typography>
                </View>
              </View>

              <View
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  paddingTop: 32,
                }}
              >
                <Button
                  // type="warning"
                  style={{
                    width: '100%',
                    backgroundColor: '#fff',
                    borderColor: colorConstant.blue,
                  }}
                  onPress={() => {}}
                >
                  <Typography color={colorConstant.blue} fontSize={14} fontWeight={'600'}>
                    Detail
                  </Typography>
                </Button>
              </View>
            </View>
          ))}
        </CollapsibleScrollView>
      )}
    </>
  )
}

export default React.memo(Absence)

const styles = StyleSheet.create({
  absenceContainer: {
    display: 'flex',
    flex: 1,
    backgroundColor: colorConstant.bg,
  },

  addWraper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    paddingHorizontal: 24,
  },
  leftAddWraper: {
    display: 'flex',
    flexDirection: 'row',

    paddingVertical: 40,
    alignItems: 'center',
  },

  rightAddWraper: {
    backgroundColor: colorConstant.red,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '40%',
    height: 42,
    borderRadius: 5,
  },
})
