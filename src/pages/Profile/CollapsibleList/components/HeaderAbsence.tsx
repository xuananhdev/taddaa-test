import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import React from 'react'
import colorConstant from '../../../../constants/colors'
import Typography from '~components/Base/Typography'

import ArrounfLeftIcon from '../../../../access/icons/arroundLeft.svg'
import CongIcon from '../../../../access/icons/cong.svg'

const HeaderAbsence = () => {
  return (
    <View style={styles.addWraper}>
      <TouchableOpacity style={styles.leftAddWraper}>
        <Typography paddingRight={12}>Absence details</Typography>
        <ArrounfLeftIcon />
      </TouchableOpacity>

      <TouchableOpacity style={styles.rightAddWraper}>
        <View style={{}}>
          <CongIcon />
        </View>

        <Typography marginLeft={10} color={'#fff'}>
          Add new
        </Typography>
      </TouchableOpacity>
    </View>
  )
}

export default HeaderAbsence

const styles = StyleSheet.create({
  addWraper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    paddingHorizontal: 24,
    paddingVertical: 20,
  },
  leftAddWraper: {
    display: 'flex',
    flexDirection: 'row',

    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#4C75DF',
    borderRadius: 5,
    height: 42,
    width: '48%',
  },

  rightAddWraper: {
    backgroundColor: colorConstant.red,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '48%',
    height: 42,
    borderRadius: 5,
  },
})
