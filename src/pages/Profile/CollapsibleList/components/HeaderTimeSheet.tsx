import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import React, { useState } from 'react'
import colorConstant from '../../../../constants/colors'
import Typography from '~components/Base/Typography'

import CongIcon from '../../../../access/icons/cong.svg'
import CalendarIcon from '../../../../access/icons/calendar1.svg'

import MonthPicker from './timePicker'
import moment from 'moment'

const HeaderProject = ({ setStartDateTimeSheet, startDateTimeSheet }: any) => {
  const [isShowModal, setShowModal] = useState(false)

  return (
    <>
      <View style={styles.addWraper}>
        <TouchableOpacity
          style={{
            display: 'flex',
            flexDirection: 'row',
            marginVertical: 25,
            height: 42,
            alignItems: 'center',
            justifyContent: 'space-between',
            borderWidth: 1,
            borderColor: colorConstant.line,
            borderRadius: 5,
            width: '65%',
            paddingHorizontal: 8,
          }}
          onPress={() => {
            setShowModal(!isShowModal)
          }}
        >
          <View style={styles.leftAddWraper}>
            <Typography>{moment(startDateTimeSheet).format('MMM YYYY')}</Typography>
          </View>

          <View style={{ paddingLeft: 10 }}>
            <CalendarIcon />
          </View>
        </TouchableOpacity>

        <TouchableOpacity style={styles.rightAddWraper}>
          <View style={{}}>
            <CongIcon />
          </View>

          <Typography marginLeft={10} color={'#fff'}>
            Add new
          </Typography>
        </TouchableOpacity>
      </View>
      <MonthPicker
        isShowModal={isShowModal}
        setShowModal={setShowModal}
        setStartDate={setStartDateTimeSheet}
        setEndDate={null}
        startDate={startDateTimeSheet}
        endDate={null}
      />
    </>
  )
}

export default React.memo(HeaderProject)

const styles = StyleSheet.create({
  addWraper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    paddingHorizontal: 24,
  },
  leftAddWraper: {
    display: 'flex',
    flexDirection: 'row',
    paddingVertical: 5,
    alignItems: 'center',
  },

  rightAddWraper: {
    backgroundColor: colorConstant.red,
    display: 'flex',
    width: '30%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 42,
    borderRadius: 5,
  },
})
