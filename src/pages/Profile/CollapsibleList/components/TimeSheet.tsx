import { StyleSheet, View, TouchableOpacity, Image } from 'react-native'
import React, { useEffect, useState } from 'react'
import colorConstant from '../../../../constants/colors'
import Typography from '~components/Base/Typography'
import { Button } from '@ant-design/react-native'

import { useSelector } from 'react-redux'
import { appUserSelector } from '~store/userSlice/selector'
import moment from 'moment'

import timeSheetServices from '~/services/axios/timeSheet'
import { TimeSheetType } from '~store/timeSheet/type'

import { CollapsibleScrollView } from '@r0b0t3d/react-native-collapsible'

const enumCategory = ['AAV', 'AFV']
const enumReviewStatus = ['Pending', 'Inprogress', 'Approved', 'Rejected']

const Absence = ({ startDateTimeSheet }: any) => {
  const [listTimeSheet, setListTimeSheet] = useState([])
  const [isLoading, setLoading] = useState(false)

  const { employeeId } = useSelector(appUserSelector)

  useEffect(() => {
    async function getProjectAllocation() {
      try {
        setLoading(true)
        const res = await timeSheetServices.getTimeSheets(employeeId, startDateTimeSheet)
        setListTimeSheet(res.data.data.eOffice_Timesheet.items)
        setLoading(false)
      } catch (error) {
        setLoading(false)
        console.log('get time sheet error', error)
      }
    }

    if (startDateTimeSheet && employeeId) {
      getProjectAllocation()
    }
  }, [employeeId, startDateTimeSheet])

  const checkApprove = (bag: any): any => {
    let approveType = ''
    for (let i = 0; i < bag.contentItems.length; i++) {
      if (bag.contentItems[i].status == 2) {
        for (let j = 0; j < bag.contentItems[i].bag.contentItems.length; j++) {
          if (bag.contentItems[i].bag.contentItems[j].isCurrent) {
            approveType = bag.contentItems[i].bag.contentItems[j].employee.contentItems[0].fullName
          } else {
            continue
          }
        }
      } else {
        continue
      }
    }

    return approveType
  }

  return (
    <>
      {isLoading ? (
        <View style={{ display: 'flex', flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Image
            style={{ width: 100, height: 100 }}
            source={require('../../../../access/images/tadaa.gif')}
            resizeMode="contain"
          />
        </View>
      ) : (
        <CollapsibleScrollView>
          {listTimeSheet.map((item: TimeSheetType, index) => (
            <View
              style={{
                backgroundColor: colorConstant.white,
                marginTop: 18,
                paddingVertical: 30,
                paddingHorizontal: 24,
              }}
              key={index.toString()}
            >
              <TouchableOpacity
                style={{
                  height: 34,
                  width: '50%',
                  backgroundColor: colorConstant.orange,
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 5,
                }}
              >
                <Typography color={colorConstant.white}>{enumReviewStatus[item.status]}</Typography>
              </TouchableOpacity>

              <View style={{ display: 'flex', flexDirection: 'row', paddingTop: 32 }}>
                <View style={{ flex: 2 }}>
                  <Typography color={colorConstant.dark} fontWeight={'600'}>
                    Month
                  </Typography>
                  <Typography paddingTop={8} color={colorConstant.dark} fontWeight={'600'}>
                    Ogranization
                  </Typography>
                  <Typography paddingVertical={8} color={colorConstant.dark} fontWeight={'600'}>
                    Total hours
                  </Typography>
                  <Typography color={colorConstant.dark} fontWeight={'600'}>
                    Approved by
                  </Typography>
                </View>

                <View style={{ flex: 3 }}>
                  <Typography color={colorConstant.dark}>
                    {moment(item.month).format('MMMM YYYY')}
                  </Typography>
                  <Typography paddingTop={8} color={colorConstant.dark}>
                    {enumCategory[item.category]}
                  </Typography>
                  <Typography paddingVertical={8} color={colorConstant.dark}>
                    {item.totalHours}
                  </Typography>
                  <Typography color={colorConstant.dark}>{checkApprove(item.bag)}</Typography>
                </View>
              </View>

              <View
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  paddingTop: 32,
                }}
              >
                <Button
                  // type="warning"
                  style={{
                    width: '45%',
                    backgroundColor: '#fff',
                    borderColor: colorConstant.blue,
                  }}
                  onPress={() => {}}
                >
                  <Typography color={colorConstant.blue} fontSize={14} fontWeight={'600'}>
                    Detail
                  </Typography>
                </Button>
                <Button
                  // type="warning"
                  style={{
                    width: '45%',
                    backgroundColor: colorConstant.white,
                    borderColor: colorConstant.red,
                  }}
                  onPress={() => {}}
                >
                  <Typography color={colorConstant.red} fontSize={14} fontWeight={'600'}>
                    Cancel
                  </Typography>
                </Button>
              </View>
            </View>
          ))}
        </CollapsibleScrollView>
      )}
    </>
  )
}

export default React.memo(Absence)

const styles = StyleSheet.create({
  absenceContainer: {
    display: 'flex',
    flex: 1,
    backgroundColor: colorConstant.bg,
  },

  addWraper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    paddingHorizontal: 24,
  },
  leftAddWraper: {
    display: 'flex',
    flexDirection: 'row',

    alignItems: 'center',
  },

  rightAddWraper: {
    backgroundColor: colorConstant.red,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '40%',
    height: 42,
    borderRadius: 5,
  },
})
