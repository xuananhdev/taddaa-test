import React from 'react'
import {
    StyleSheet,
    Animated,
    View,
    Image,
    TouchableOpacity,
    ScrollView,
    Dimensions,
} from 'react-native'
import colorConstant from '../../constants/colors'

import { Button, ActivityIndicator } from '@ant-design/react-native'
import Typography from '~components/Base/Typography'

import { useSelector } from 'react-redux'
import { appUserSelector } from '~store/userSlice/selector'
import type { NativeStackScreenProps } from '@react-navigation/native-stack'

import type { ProfileStackParamList } from '~routes/types'
import moment from 'moment'
import HeaderWithOnlyBack from '~components/Header/HeaderWithOnlyBack'

type Props = NativeStackScreenProps<ProfileStackParamList, 'ProfileDetail'>

const Detail = ({ route, navigation }: Props) => {
    const { avatarUrl, lastName } = useSelector(appUserSelector)
    const { position, phoneNumber, email, address, username, startDate, department, hourPerDay } =
        route.params
    return (
        <View style={{ display: 'flex', flex: 1 }}>
            <HeaderWithOnlyBack navigation={navigation} />
            {/*  */}

            <ScrollView>
                <Typography
                    fontSize={26}
                    fontWeight="600"
                    color={colorConstant.dark}
                    paddingVertical={20}
                    paddingLeft={24}
                ></Typography>

                <View style={styles.detailContainer}>
                    <View style={styles.detailWraper}>
                        <View style={styles.profileImageWraper}>
                            <View style={styles.ImageWraper}>
                                {avatarUrl ? (
                                    <Image
                                        style={styles.profileImage}
                                        source={{
                                            uri: avatarUrl,
                                        }}
                                        resizeMode="contain"
                                    />
                                ) : (
                                        <View
                                            style={{
                                                ...styles.profileImage,
                                                backgroundColor: 'red',
                                                display: 'flex',
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                            }}
                                        >
                                            <Typography fontSize={30} fontWeight={'600'} color="#fff">
                                                {lastName.slice(0, 1)}
                                            </Typography>
                                        </View>
                                    )}
                            </View>
                        </View>

                        <View style={{ display: 'flex', flex: 1, justifyContent: 'flex-start', marginTop: 24 }}>
                            <Typography
                                color={colorConstant.black}
                                fontSize={18}
                                lineHeight={19}
                                fontWeight={'600'}
                            >
                                Chu Thi Ha
            </Typography>
                        </View>
                    </View>

                    <View>
                        <Typography color={colorConstant.black} fontWeight={'600'} paddingTop={16}>
                            Position
          </Typography>
                        <Typography color={colorConstant.black} paddingTop={4}>
                            {position}
                        </Typography>
                    </View>

                    <View>
                        <Typography color={colorConstant.black} fontWeight={'600'} paddingTop={16}>
                            Mobile
          </Typography>
                        <Typography color={colorConstant.black} paddingTop={4}>
                            {phoneNumber}
                        </Typography>
                    </View>

                    <View>
                        <Typography color={colorConstant.black} fontWeight={'600'} paddingTop={16}>
                            Email
          </Typography>
                        <Typography color={colorConstant.black} paddingTop={4}>
                            {email}
                        </Typography>
                    </View>

                    <View>
                        <Typography color={colorConstant.black} fontWeight={'600'} paddingTop={16}>
                            Address
          </Typography>
                        <Typography color={colorConstant.black} paddingTop={4}>
                            {address}
                        </Typography>
                    </View>
                    {/* Line */}
                    <View
                        style={{
                            width: '100%',
                            height: 1,
                            backgroundColor: colorConstant.line,
                            marginVertical: 35,
                        }}
                    />
                    {/*  */}
                    <View style={{ display: 'flex', flexDirection: 'row' }}>
                        <View style={{ flex: 2 }}>
                            <Typography color={colorConstant.dark} fontWeight={'600'}>
                                Account name
            </Typography>
                            <Typography paddingVertical={8} color={colorConstant.dark} fontWeight={'600'}>
                                Report to
            </Typography>
                            <Typography paddingVertical={8} color={colorConstant.dark} fontWeight={'600'}>
                                Start date
            </Typography>
                            <Typography paddingVertical={8} color={colorConstant.dark} fontWeight={'600'}>
                                Employee type
            </Typography>
                            <Typography paddingVertical={8} color={colorConstant.dark} fontWeight={'600'}>
                                Department
            </Typography>
                            <Typography paddingVertical={8} color={colorConstant.dark} fontWeight={'600'}>
                                Number of hours/day
            </Typography>
                        </View>

                        <View style={{ flex: 3, marginLeft: 10 }}>
                            <Typography color={colorConstant.dark}>ha.chuthi</Typography>
                            <Typography paddingVertical={8} color={colorConstant.dark}>
                                {username}
                            </Typography>
                            <Typography paddingVertical={8} color={colorConstant.dark}>
                                {moment(startDate).format('DD/MM/YYYY')}
                            </Typography>
                            <Typography paddingVertical={8} color={colorConstant.dark}>
                                Fixed term
            </Typography>
                            <Typography paddingVertical={8} color={colorConstant.dark}>
                                {department}
                            </Typography>
                            <Typography paddingVertical={8} color={colorConstant.dark}>
                                {hourPerDay} hour(s)
            </Typography>
                        </View>
                    </View>
                    {/* Button */}
                    <View style={styles.buttonWraper}>
                        <Button
                            // type="warning"
                            style={{
                                width: '100%',
                                backgroundColor: '#fff',
                                borderColor: colorConstant.blue,
                            }}
                            onPress={() => { }}
                        >
                            <Typography color={colorConstant.blue} fontSize={14} fontWeight={'600'}>
                                Edit
            </Typography>
                        </Button>
                    </View>
                </View>
            </ScrollView>

        </View>
    )
}

export default React.memo(Detail)

const styles = StyleSheet.create({
    detailContainer: {
        display: 'flex',
        flex: 1,
        backgroundColor: '#fff',
        paddingHorizontal: 25,
        paddingBottom: 28,
    },
    detailWraper: {
        display: 'flex',
        flexDirection: 'row',
    },
    profileImageWraper: {
        display: 'flex',
        flex: 1,
        flexDirection: 'row',
        // position: 'relative',
    },
    ImageWraper: {
        // position: 'absolute',
        top: -20,
        left: 30,
        width: 111,
        height: 111,
        borderWidth: 11,
        borderColor: '#fff',
        borderRadius: 50,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    profileImage: {
        width: 100,
        height: 100,
        borderRadius: 50,
    },
    buttonWraper: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: 27,
    },
})
