import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { useSelector } from 'react-redux'
import { appUserSelector } from '~store/userSlice/selector'

const Wellcome = () => {
  const { fullName } = useSelector(appUserSelector)
  return (
    <>
      <Text style={styles.wellcome}>Welcome, {fullName}</Text>
      <Text style={styles.tabLabel}>Dashboard</Text>
    </>
  )
}

export default React.memo(Wellcome)

const styles = StyleSheet.create({
  wellcome: {
    color: '#5F5F5F',
    fontSize: 16,
    fontWeight: '600',
    paddingTop: 38,
  },
  tabLabel: { fontWeight: '600', fontSize: 26, color: '#323232', paddingBottom: 20 },
  dashboardContainer: {
    display: 'flex',
    flex: 1,
  },
})
