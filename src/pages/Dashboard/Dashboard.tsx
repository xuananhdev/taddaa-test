import React, { useEffect } from 'react'
import { Text, View, StyleSheet } from 'react-native'
import TabsDashBoard from './Tabs/Tabs'

import Header from '../../components/Header/Header'
import Wellcome from './Wellcome/Wellcome'

import type { RootStackParamList } from '~routes/types'
import type { NativeStackScreenProps } from '@react-navigation/native-stack'

// export interface OwnProps {
//   handleIdChange: (newId: number) => void;
// }

// interface Props extends OwnProps {
//   data: useTaskQuery;
// }

type Props = NativeStackScreenProps<RootStackParamList, 'Dashboard'>

const Dashboard = ({ navigation, route }: Props) => {
  return (
    <View style={styles.dashboardContainer}>
      <Header navigation={navigation} />

      <View style={{ flex: 1, paddingHorizontal: 24 }}>
        <Wellcome />
        {/* tabs */}
        <TabsDashBoard />
      </View>
    </View>
  )
}

export default React.memo(Dashboard)

const styles = StyleSheet.create({
  tabLabel: { fontWeight: '600', fontSize: 26, color: '#323232' },
  dashboardContainer: {
    display: 'flex',
    flex: 1,
  },
})
