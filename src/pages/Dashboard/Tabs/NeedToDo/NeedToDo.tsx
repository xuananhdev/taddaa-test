import { StyleSheet, Text, View, Image, TouchableOpacity, FlatList } from 'react-native'
import React, { useState, useEffect } from 'react'
import CheckedIcon from '../../../../access/icons/checked.svg'
import StarIcon from '../../../../access/icons/star.svg'
import CalendarIcon from '../../../../access/icons/calendar.svg'
import moment from 'moment'
import taskServices from '~/services/axios/task'
import { ActivityIndicator } from '@ant-design/react-native'

import { useDispatch, useSelector } from 'react-redux'
import { setPriority, setMarkAsDone } from '~store/taskSlice'

const NeedToDo = (props: any) => {
  // const [isLoadingTodoTab, setLoadingToDoTab] = useState(true)

  const dispatch = useDispatch()

  const _setPriority = async (item: any) => {
    try {
      const res = await taskServices.setPriority(item.contentItemId)
      dispatch(setPriority({ ...item, myPriority: true }))
    } catch (error) {
      console.log('setPriority Error: ', error)
    }
  }

  const _MarkAsDone = async (item: any) => {
    try {
      const res = await taskServices.setMarkAsDone(item.contentItemId)
      dispatch(setMarkAsDone({ ...item, isDone: true, screen: 'needToDo' }))
    } catch (error) {
      console.log('setPriority Error: ', error)
    }
  }

  return (
    <View style={{ flex: 1, marginHorizontal: 24 }}>
      {!props.isFetchingTask ? (
        <View style={{ flex: 1 }}>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={props.listTodo}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => (
              <View style={styles.cardContainer}>
                <Text style={styles.event}>{item.rootRelatedContentType}</Text>

                <View style={styles.timeEventWrapper}>
                  <View style={{ display: 'flex', flex: 1 }}>
                    <Text style={styles.description}>{item.name}</Text>
                  </View>

                  <View
                    style={{
                      display: 'flex',
                      flexDirection: 'row',

                      justifyContent: 'center',
                      alignItems: 'center',
                      height: '100%',
                    }}
                  >
                    {item.dueDate && (
                      <View style={styles.timeWrapper}>
                        <CalendarIcon />
                        <Text style={styles.time}>{moment(item.dueDate).format('DD MMMM')}</Text>
                      </View>
                    )}
                  </View>
                </View>

                <View style={styles.actionEventWrapper}>
                  <TouchableOpacity
                    style={{
                      ...styles.markDoneWrapper,
                      backgroundColor: item?.isDone ? '#67c465' : '#fff',
                    }}
                    onPress={() => {
                      _MarkAsDone(item)
                    }}
                  >
                    {item?.isDone ? <StarIcon /> : <CheckedIcon />}

                    <Text
                      style={{ ...styles.actionTitle, color: item?.isDone ? '#fff' : '#67c465' }}
                    >
                      {item?.isDone ? 'Done' : 'Mark as done'}
                    </Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={{ ...styles.markDoneWrapper, marginLeft: 20, borderColor: '#E2C251' }}
                    onPress={() => {
                      _setPriority(item)
                    }}
                  >
                    <StarIcon />
                    <Text style={{ ...styles.actionTitle, color: '#E2C251' }}>My priority</Text>
                  </TouchableOpacity>
                </View>
              </View>
            )}
          />
        </View>
      ) : (
        <View style={{ display: 'flex', flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Image
            style={{ width: 100, height: 100 }}
            source={require('../../../../access/images/tadaa.gif')}
            resizeMode="contain"
          />
        </View>
      )}
    </View>
  )
}

export default React.memo(NeedToDo)

const styles = StyleSheet.create({
  actionTitle: {
    color: '#67C365',
    fontWeight: '600',
  },
  dot: {
    width: 6,
    height: 30,
    backgroundColor: '#E3253C',
    borderRadius: 8,
  },
  timeWrapper: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#E3253C',
    padding: 10,
    borderRadius: 5,
  },
  time: {
    paddingLeft: 5,
    color: '#fff',
  },
  event: {
    color: '#4C75DF',
    fontSize: 14,
  },
  description: {
    paddingBottom: 13,
    lineHeight: 29,
    fontSize: 16,
  },
  cardContainer: {
    borderBottomColor: '#D3D6D9',
    borderBottomWidth: 1,
    paddingBottom: 10,
    paddingTop: 25,
  },

  summaryNum: {
    fontSize: 16,
    fontWeight: '600',
  },
  summaryDes: {
    fontSize: 14,
    fontWeight: '400',
    lineHeight: 19,
    color: '#323232',
  },
  timeEventWrapper: {
    display: 'flex',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  actionEventWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    color: '#CFC365',
    paddingBottom: 20,
    paddingTop: 16,
    justifyContent: 'space-between',
  },
  markDoneWrapper: {
    display: 'flex',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#67C365',
    paddingVertical: 10,
  },
})
