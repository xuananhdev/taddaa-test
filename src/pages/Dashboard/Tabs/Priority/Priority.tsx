import { StyleSheet, Text, View, Image, TouchableOpacity, FlatList } from 'react-native'
import React from 'react'
import CheckedIcon from '../../../../access/icons/checked.svg'
import StarIcon from '../../../../access/icons/star.svg'

import { ActivityIndicator } from '@ant-design/react-native'
import moment from 'moment'
import taskServices from '~/services/axios/task'

import { useDispatch } from 'react-redux'
import { setMarkAsDone } from '~store/taskSlice'

const Priority = (props: any) => {
  const dispatch = useDispatch()

  const _MarkAsDone = async (item: any) => {
    try {
      const res = await taskServices.setMarkAsDone(item.contentItemId)
      dispatch(setMarkAsDone({ ...item, isDone: true, screen: 'priority' }))
    } catch (error) {
      console.log('setPriority Error: ', error)
    }
  }

  return (
    <View style={{ flex: 1, marginHorizontal: 24 }}>
      {!props.isFetchingTask ? (
        <View style={{ flex: 1 }}>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={props.listPriority}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => (
              <View style={styles.cardContainer}>
                <Text style={styles.event}>{item.rootRelatedContentType}</Text>

                <View style={styles.timeEventWrapper}>
                  <View style={{ display: 'flex', flex: 2 }}>
                    <Text style={styles.description}>{item.name}</Text>
                  </View>

                  {item.dueDate && (
                    <View
                      style={{
                        display: 'flex',
                        flex: 1,
                        alignItems: 'center',
                        height: '100%',
                      }}
                    >
                      <Text style={styles.time}>{moment(item.dueDate).format('DD MMMM')}</Text>
                    </View>
                  )}
                </View>

                <View style={styles.actionEventWrapper}>
                  <TouchableOpacity
                    style={{
                      ...styles.markDoneWrapper,
                      backgroundColor: item?.isDone ? '#67c465' : '#fff',
                    }}
                    onPress={() => {
                      _MarkAsDone(item)
                    }}
                  >
                    {item?.isDone ? <StarIcon /> : <CheckedIcon />}
                    <Text
                      style={{ ...styles.actionTitle, color: item?.isDone ? '#fff' : '#67c465' }}
                    >
                      {item?.isDone ? 'Done' : 'Mark as done'}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            )}
          />
        </View>
      ) : (
        <View style={{ display: 'flex', flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Image
            style={{ width: 100, height: 100 }}
            source={require('../../../../access/images/tadaa.gif')}
            resizeMode="contain"
          />
        </View>
      )}
    </View>
  )
}

export default React.memo(Priority)

const styles = StyleSheet.create({
  actionTitle: {
    color: '#67C365',
    fontWeight: '600',
    marginLeft: 7,
  },
  dot: {
    width: 6,
    height: 30,
    backgroundColor: '#E3253C',
    borderRadius: 8,
  },
  time: {
    backgroundColor: '#E3253C',
    padding: 5,
    borderRadius: 5,
    color: '#fff',
    // textAlign: 'center',
  },
  event: {
    color: '#4C75DF',
    fontSize: 14,
  },
  description: {
    paddingBottom: 13,
    lineHeight: 29,
    fontSize: 16,
  },
  cardContainer: {
    borderBottomColor: '#D3D6D9',
    borderBottomWidth: 1,
    paddingBottom: 10,
    paddingTop: 25,
  },
  summaryNum: {
    fontSize: 16,
    fontWeight: '600',
  },
  summaryDes: {
    fontSize: 14,
    fontWeight: '400',
    lineHeight: 19,
    color: '#323232',
  },
  timeEventWrapper: {
    display: 'flex',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  actionEventWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    color: '#CFC365',
    paddingBottom: 20,
    paddingTop: 16,
  },
  markDoneWrapper: {
    display: 'flex',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#67C365',
    paddingVertical: 10,
  },
})
