import { StyleSheet, Text, View, useWindowDimensions } from 'react-native'
import React, { useEffect, useState } from 'react'
import { TabView, SceneMap, TabBar } from 'react-native-tab-view'
import NeedTodo from './NeedToDo/NeedToDo'
import Priority from './Priority/Priority'

import { useDispatch, useSelector } from 'react-redux'
import { saveTask } from '~store/taskSlice'
import { taskPrioritySelector, taskTodoSelector } from '~store/taskSlice/selector'
import { appUserSelector } from '~store/userSlice/selector'

import taskServices from '../../../services/axios/task'

type TaskProps = {
  myPriority: boolean
}

const renderTabBar = (props: any) => (
  <TabBar
    {...props}
    indicatorStyle={{ backgroundColor: '#E3253C' }}
    style={{
      backgroundColor: 'transparent',
      shadowOffset: { height: 0, width: 0 },
      shadowColor: 'transparent',
      shadowOpacity: 0,
      elevation: 0,
    }}
    pressColor={'transparent'}
    pressOpacity={undefined}
    // labelStyle={{ color: '#5F5F5F' }}
    renderLabel={({ route, focused, color }) => (
      <View
        style={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Text style={{ color: focused ? '#323232' : '#5F5F5F', margin: 8 }}>{route.title}</Text>
        <View
          style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            width: 22,
            height: 22,
            borderRadius: 11,
            backgroundColor: route.key == 'first' ? '#FFECF0' : '#E9F6E9',
            marginLeft: 6,
          }}
        >
          <Text style={{ color: route.key == 'first' ? '#E3253C' : '#67C365', fontWeight: '600' }}>
            {route.key == 'first' ? props.todoCount : props.priorityCount}
          </Text>
        </View>
      </View>
    )}
  />
)

const Tabs = () => {
  const layout = useWindowDimensions()

  const dispatch = useDispatch()
  const { employeeId } = useSelector(appUserSelector)
  const [isFetchingTask, setFetchingTask] = useState(false)

  useEffect(() => {
    async function getTask() {
      if (employeeId) {
        try {
          setFetchingTask(true)
          const resListTask = await taskServices.getListTask(employeeId)
          const listTask = resListTask?.data?.data?.eOffice_Task?.items
          if (listTask) {
            const todo = listTask.filter((task: TaskProps) => task.myPriority === false)
            const priority = listTask.filter((task: TaskProps) => task.myPriority === true)
            setFetchingTask(false)
            dispatch(saveTask({ needTodo: todo, priority }))
          }
        } catch (error) {
          console.log('getListTask error', error)
        }
      }
    }
    getTask()
  }, [employeeId])

  const listTaskTodoSelector = useSelector(taskTodoSelector)

  const listTaskPrioritySelector = useSelector(taskPrioritySelector)

  const renderScene = SceneMap({
    first: () => <NeedTodo listTodo={listTaskTodoSelector} isFetchingTask={isFetchingTask} />,
    second: () => (
      <Priority listPriority={listTaskPrioritySelector} isFetchingTask={isFetchingTask} />
    ),
  })

  const [index, setIndex] = React.useState(0)
  const [routes] = React.useState([
    { key: 'first', title: 'Need to do' },
    { key: 'second', title: 'My Priority' },
  ])

  return (
    <TabView
      navigationState={{ index, routes }}
      renderScene={renderScene}
      onIndexChange={setIndex}
      renderTabBar={(props) =>
        renderTabBar({
          ...props,
          // Custom props
          todoCount: listTaskTodoSelector?.length,
          priorityCount: listTaskPrioritySelector?.length,
        })
      }
      initialLayout={{ width: layout.width }}
      style={{
        marginTop: 16,
        backgroundColor: '#FFFFFF',
      }}
    />
  )
}

export default React.memo(Tabs)

const styles = StyleSheet.create({})
