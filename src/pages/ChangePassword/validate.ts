// App.js
import * as yup from 'yup'

const loginValidationSchema = yup.object().shape({
  newPassword: yup
    .string()
    .min(6, ({ min }) => `Code must be at least ${min} characters`)
    .required('Code is required'),

  confirmNewPassword: yup
    .string()
    .oneOf([yup.ref('newPassword'), null], 'new passwords must match'),
})

export default loginValidationSchema
