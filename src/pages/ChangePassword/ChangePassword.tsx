import {
  StyleSheet,
  Text,
  View,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Platform,
  Keyboard,
  StatusBar,
} from 'react-native'
import React, { useState } from 'react'
import { Button, ActivityIndicator } from '@ant-design/react-native'
import { Input } from '~components/index'

import { Formik, Form, ErrorMessage } from 'formik'
import ChangePasswordScheme from './validate'
import authService from '~/services/auth'
import Dialog from 'react-native-dialog'
import type { RootStackParamList } from '~routes/types'
import type { NativeStackScreenProps } from '@react-navigation/native-stack'

import PasswordIcon from '../../access/icons/password.svg'
import CircleSolidIcon from '../../access/icons/CircleSolidIcon'

import { useHeaderHeight } from '@react-navigation/elements'

type Props = NativeStackScreenProps<RootStackParamList, 'ChangePassword'>

const ChangePassword = ({ route, navigation }: Props) => {
  const [visible, setVisible] = useState(false)
  const [isSubmit, setSubmit] = useState(false)

  const headerHeight = useHeaderHeight()
  const statusBarHeight = StatusBar.currentHeight || 0
  const KEYBOARD_VERTICAL_OFFSET = headerHeight + statusBarHeight + 30

  const _resetPassword = async (newPassword: string) => {
    const { email, code } = route.params

    if (email && code && newPassword) {
      try {
        setSubmit(true)
        const resResetPassword = await authService.resetPassword(email, newPassword, code)
        setSubmit(false)
        navigation.replace('LoginUserName')
      } catch (error) {
        setVisible(true)
        console.log('resResetPassword error: ', error)
      }
    }
  }

  return (
    <>
      <Formik
        validationSchema={ChangePasswordScheme}
        initialValues={{ newPassword: '', confirmNewPassword: '' }}
        onSubmit={(values) => console.log('onSubmit::', values)}
      >
        {({ handleChange, handleBlur, handleSubmit, values, errors, isValid }) => (
          <KeyboardAvoidingView
            behavior={Platform.OS === 'ios' ? 'padding' : undefined}
            style={{ display: 'flex', flex: 1 }}
            keyboardVerticalOffset={KEYBOARD_VERTICAL_OFFSET}
            // enabled={Platform.OS === 'ios' ? true : false}
          >
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
              <View style={styles.authContainer}>
                <Text style={styles.heading}>Login</Text>
                <Text style={styles.description}>Enter your password to log in</Text>

                <Text style={styles.label}>
                  New Password<Text style={styles.specicalText}>*</Text>
                </Text>
                <Input
                  LeftIcon={<PasswordIcon />}
                  placeholder={'Enter your new password'}
                  onChangeText={handleChange('newPassword')}
                  value={values.newPassword}
                  error={errors.newPassword ? true : false}
                  autoFocus={false}
                  secureTextEntry={true}
                />
                <Text style={styles.label}>
                  Confirm New Password<Text style={styles.specicalText}>*</Text>
                </Text>
                <Input
                  LeftIcon={<PasswordIcon />}
                  placeholder={'Confirm your new password'}
                  onChangeText={handleChange('confirmNewPassword')}
                  value={values.confirmNewPassword}
                  error={errors.confirmNewPassword ? true : false}
                  autoFocus={false}
                  secureTextEntry={true}
                />

                <View style={styles.buttonWraper}>
                  <Button
                    disabled={
                      !values.newPassword ||
                      !values.confirmNewPassword ||
                      errors.newPassword ||
                      errors.confirmNewPassword
                        ? true
                        : false
                    }
                    type="warning"
                    style={{
                      width: '100%',
                      backgroundColor: '#E3253C',
                    }}
                    onPress={() => {
                      _resetPassword(values.newPassword)
                    }}
                  >
                    {isSubmit ? <ActivityIndicator color="#fff" /> : 'Change password'}
                  </Button>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </KeyboardAvoidingView>
        )}
      </Formik>

      <Dialog.Container
        visible={visible}
        contentStyle={{
          display: 'flex',
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
          paddingHorizontal: 10
        }}
      >
        <View style={styles.dialogContainer}>
          <View
            style={{
              display: 'flex',
              flexDirection: 'row',
              paddingBottom: 25,
              alignItems: 'center',
            }}
          >
            <CircleSolidIcon />
            <Text style={{ marginLeft: 6 }}>
              The specifield usernam/password couple is invailid.
            </Text>
          </View>

          <View style={{ width: '100%' }}>
            <Dialog.Button
              label="Ok"
              // Reset naviagtion remove all stack
              onPress={() => {
                setVisible(false)
                setSubmit(false)
              }}
              style={{
                width: 300,
              }}
            />
          </View>
        </View>
      </Dialog.Container>
    </>
  )
}

export default React.memo(ChangePassword)

const styles = StyleSheet.create({
  //diaglog
  dialogContainer: {
    // backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  //
  specicalText: { color: '#E3253C' },
  authContainer: {
    display: 'flex',
    flex: 1,
    paddingHorizontal: 24,
  },
  heading: {
    color: '#323232',
    fontWeight: '700',
    fontSize: 26,
    lineHeight: 36,
  },
  description: {
    fontSize: 16,
    lineHeight: 22,
    color: '#5F5F5F',
  },
  label: {
    fontSize: 16,
    fontWeight: '600',
    paddingTop: 32,
    paddingBottom: 12,
  },
  buttonWraper: {
    position: 'absolute',
    bottom: 0,
    left: 24,
    right: 24,
    marginBottom: 30,
  },
  forgotPass: {
    color: '#E3253C',
    paddingTop: 16,
    fontSize: 16,
    fontWeight: '600',
  },
})
