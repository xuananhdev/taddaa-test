import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  root: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
  },
  text: {
    color: 'red',
    fontSize: 18,
    fontWeight: 'bold',
  },
  splashImg: {
    width: '100%',
    height: '100%',
  },
})

export default styles
