import React, { useState, useEffect } from 'react'
import { Text, View, StyleSheet, Dimensions, Image } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'
import authServices from '../../services/auth'

import { useDispatch } from 'react-redux'
import { reSetState } from '~store/userSlice'

import configConstants from '~/configs/constants'
import Line1 from '../../access/icons/line1.svg'
import { sleep } from '../../utils/time'

import styles from './style'

const screen = Dimensions.get('screen')

const sWidth = Dimensions.get('window').width
const sHeight = Dimensions.get('window').height
const ratio = sWidth / sHeight //sWidth = ratio * sHeight
const myStyle = StyleSheet.create({
  container: {
    width: sWidth,
    height: sHeight,
    backgroundColor: '#fff',
  },
  top_background: {
    width: sHeight * 2,
    height: sHeight * 2,
    borderRadius: sHeight * 1,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    backgroundColor: '#aaa',
    alignItems: 'center',
    marginLeft: ratio * sHeight * 0.5 - sHeight,
    // marginTop: -sHeight * 1.7,
    // paddingTop: sHeight * 1.7,
  },
  top_content: {
    paddingTop: sHeight * 0.02,
    width: sWidth,
    height: sHeight * 0.3,
    alignItems: 'center',
  },
  text1: {
    fontSize: 14,
    color: '#fff',
  },
  text2: {
    marginTop: sHeight * 0.1,
    fontSize: 25,
    fontWeight: 'bold',
    color: '#fff',
  },
})

const SplashScreen = (props: any) => {
  const dispatch = useDispatch()
  const [timeAnimation, setTimeAnimation] = useState(false)

  useEffect(() => {
    setTimeout(() => {
      setTimeAnimation(true)
    }, 2300)
  }, [])

  useEffect(() => {
    if (timeAnimation) {
      async function checkToken() {
        try {
          const refreshToken = await AsyncStorage.getItem('refreshToken')

          if (refreshToken) {
            const res = await authServices.refreshToken({
              grant_type: configConstants.grant_type.refresh_token,
              client_id: configConstants.client_id,
              refresh_token: refreshToken, // get refresh token
            })
            const { access_token, refresh_token } = res.data
            if (access_token && refresh_token) {
              await AsyncStorage.setItem('accessToken', access_token)
              await AsyncStorage.setItem('refreshToken', refresh_token)
              props.navigation.navigate('DashboardDR') // replace
            } else {
              props.navigation.replace('Login')
            }
          } else {
            props.navigation.replace('Login')
          }
        } catch (error: any) {
          console.log('checkToken error', error)
          if (error?.response?.data?.error === 'invalid_grant') {
            await AsyncStorage.clear()
            await dispatch(reSetState({}))
            props.navigation.navigate('Login')
          }
        }
      }

      checkToken()
    }
  }, [timeAnimation])

  return (
    <View style={styles.root}>
      <Image
        source={require('../../access/images/splash.gif')}
        resizeMode="cover"
        style={{ width: '100%', height: '100%' }}
      />
      {/* {!timeAnimation ? (
        <Image
          source={require('../../access/images/splash1.png')}
          style={styles.splashImg}
          resizeMode="cover"
        />
      ) : (
        <Image
          source={require('../../access/images/splash.png')}
          style={styles.splashImg}
          resizeMode="cover"
        />
      )} */}
    </View>
    // <View style={myStyle.container}>
    //   <View style={{ position: 'absolute', top: 0, bottom: 0 }}>
    //     <Line1 />
    //     <View style={{ position: 'absolute', top: '50%' }}>
    //       <Line1 />
    //     </View>
    //   </View>
    // </View>
  )
}

// const styles = StyleSheet.create({
//   container: {
//     display: 'flex',
//     flex: 1,
//     position: 'relative',
//   },
// })

export default React.memo(SplashScreen)
