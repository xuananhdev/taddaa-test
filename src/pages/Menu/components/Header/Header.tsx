import { View, StyleSheet, Modal } from 'react-native'
import React, { useState } from 'react'
import { Button } from '@ant-design/react-native'
import TadaaIcon from '../../../../access/icons/logoTadaa.svg'
import BellIcon from '../../../../access/icons/bell.svg'
import Notif from '~pages/Notification/Notification'

const Header = () => {
  const [isShowModal, setShowModal] = useState(false)
  return (
    <>
      <View style={styles.hederWraper}>
        <TadaaIcon />
        <Button
          style={{ backgroundColor: 'transparent', borderColor: 'transparent' }}
          onPress={() => {
            setShowModal(true)
          }}
        >
          <View style={styles.notifIconWraper}>
            <BellIcon />
            <View style={styles.dotNotif} />
          </View>
        </Button>
      </View>

      <Modal animationType="slide" transparent={false} visible={isShowModal}>
        <Notif setShowModal={setShowModal} />
      </Modal>
    </>
  )
}

export default React.memo(Header)

const styles = StyleSheet.create({
  hederWraper: {
    backgroundColor: '#FFFFFF',
    height: 107,
    width: ' 100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 24,
    borderBottomColor: '#D3D6D9',
    borderBottomWidth: 1,
  },
  notifIconWraper: {
    position: 'relative',
  },

  dotNotif: {
    height: 10,
    width: 10,
    borderRadius: 5,
    position: 'absolute',
    right: -5,
    top: -4,
    backgroundColor: '#E3253C',
  },
})
