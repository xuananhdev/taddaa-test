import { StyleSheet, Text, View, ScrollView, TouchableOpacity } from 'react-native'
import React from 'react'
import PeopleIcon from '../../access/icons/people.svg'
import BagIcon from '../../access/icons/bag.svg'
import TvIcon from '../../access/icons/tv.svg'
import OfficeIcon from '../../access/icons/office.svg'
import LogOutIcon from '../../access/icons/logout1.svg'

import AsyncStorage from '@react-native-async-storage/async-storage'

import { useDispatch } from 'react-redux'
import { reSetState } from '~store/userSlice'

const Menu = () => {
  const dispatch = useDispatch()

  const handleLogout = async () => {
    await AsyncStorage.clear()
    dispatch(reSetState({}))
  }

  return (
    <View style={styles.memuContainer}>
      <View style={styles.menuBodyWraper}>
        <Text style={styles.memuTitle}>Menu</Text>
        <ScrollView style={styles.bodyContainer}>
          <View style={styles.sectionWraper}>
            <PeopleIcon />
            <View style={styles.sectionHeadingWraper}>
              <Text style={styles.sectionHeadingTitle}>HR Managment</Text>

              <TouchableOpacity>
                <Text style={[styles.sectionHeadingTitle, styles.sectionHeadingText]}>
                  TimeSheet
                </Text>
              </TouchableOpacity>
              <TouchableOpacity>
                <Text style={[styles.sectionHeadingTitle, styles.sectionHeadingText]}>Absence</Text>
              </TouchableOpacity>
              <TouchableOpacity>
                <Text style={[styles.sectionHeadingTitle, styles.sectionHeadingText]}>
                  Project Allocation
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.sectionWraper}>
            <BagIcon />
            <View style={styles.sectionHeadingWraper}>
              <Text style={styles.sectionHeadingTitle}>Doc In & Out</Text>

              <TouchableOpacity>
                <Text style={[styles.sectionHeadingTitle, styles.sectionHeadingText]}>
                  Document List
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.sectionWraper}>
            <TvIcon />
            <View style={styles.sectionHeadingWraper}>
              <Text style={styles.sectionHeadingTitle}>TOR</Text>
              <TouchableOpacity>
                <Text style={[styles.sectionHeadingTitle, styles.sectionHeadingText]}>
                  TOR List
                </Text>
              </TouchableOpacity>
              <TouchableOpacity>
                <Text style={[styles.sectionHeadingTitle, styles.sectionHeadingText]}>
                  Partner List
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.sectionWraper}>
            <OfficeIcon />
            <View style={styles.sectionHeadingWraper}>
              <Text style={styles.sectionHeadingTitle}>Office Management</Text>
              <TouchableOpacity>
                <Text style={[styles.sectionHeadingTitle, styles.sectionHeadingText]}>
                  Expense Claim List
                </Text>
              </TouchableOpacity>
              <TouchableOpacity>
                <Text style={[styles.sectionHeadingTitle, styles.sectionHeadingText]}>
                  Task Management
                </Text>
              </TouchableOpacity>
            </View>
          </View>

          <TouchableOpacity style={styles.sectionWraper} onPress={() => handleLogout()}>
            <LogOutIcon />
            <View style={styles.sectionHeadingWraper}>
              <Text style={styles.sectionHeadingTitle}>Logout</Text>
            </View>
          </TouchableOpacity>

          {/* <View style={styles.sectionWraper}>
            <TouchableOpacity onPress={() => {}}>
              <LogOutIcon />
              <View style={styles.sectionHeadingWraper}>
                <Text style={[styles.sectionHeadingTitle, styles.sectionHeadingText]}>Logout</Text>
              </View>
            </TouchableOpacity>
          </View> */}
        </ScrollView>
      </View>
    </View>
  )
}

export default React.memo(Menu)

const styles = StyleSheet.create({
  bodyContainer: {
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
  },
  memuContainer: {
    display: 'flex',
    flex: 1,
    backgroundColor: '#FFFFFF',
  },

  memuTitle: {
    color: '#323232',
    fontSize: 26,
    fontWeight: '600',
    paddingTop: 32,
  },
  menuBodyWraper: {
    display: 'flex',
    flex: 1,
    paddingHorizontal: 24,
  },
  sectionWraper: {
    display: 'flex',
    flexDirection: 'row',
    paddingTop: 32,
  },
  sectionHeadingWraper: {
    display: 'flex',
    flexDirection: 'column',
    paddingLeft: 13,
  },
  sectionHeadingTitle: {
    fontSize: 16,
    fontWeight: '400',
    color: '#323232',
  },
  sectionHeadingText: {
    color: '##5F5F5F',
    paddingTop: 16,
  },
})
