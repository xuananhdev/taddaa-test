import {
  StyleSheet,
  Text,
  View,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Platform,
  Keyboard,
  StatusBar,
} from 'react-native'
import React, { useState, useEffect } from 'react'
import { Button, ActivityIndicator } from '@ant-design/react-native'
import { Input } from '~components/index'
import { Formik, Form, ErrorMessage } from 'formik'
import ForgotPasswordScheme from './validate'
import authService from '~services/auth'
import Dialog from 'react-native-dialog'
import type { RootStackNavigationProps } from '~routes/types'
import { useNavigation } from '@react-navigation/native'

import EmailIcon from '../../access/icons/email.svg'

import { useHeaderHeight } from '@react-navigation/elements'

const ForgotPassword = () => {
  const navigation = useNavigation<RootStackNavigationProps<'ForgotPassword'>>()
  const [visible, setVisible] = useState(false)
  const [isSubmit, setSubmit] = useState<Boolean | false>()

  const headerHeight = useHeaderHeight()
  const statusBarHeight = StatusBar.currentHeight || 0
  const KEYBOARD_VERTICAL_OFFSET = headerHeight + statusBarHeight + 30

  const handleForgotPassword = async (email: string) => {
    try {
      setSubmit(true)
      const resForgot = await authService.forgotPassword(email)
      // const resForgot = await authService.forgotPassword('trang.phamthu@actionaid.org')
      // console.log('resForgot::', resForgot)
      if (resForgot.status == 204) {
        setSubmit(false)
        navigation.navigate('VerifyCode', { email })
      }
    } catch (error) {
      setVisible(true)
      console.log('forgotPassword error: ', error)
      setSubmit(false)
    }
  }

  return (
    <>
      <Formik
        validationSchema={ForgotPasswordScheme}
        initialValues={{ email: '' }}
        onSubmit={(values) => console.log('onSubmit::', values)}
      >
        {({ handleChange, handleBlur, handleSubmit, values, errors, isValid }) => (
          <KeyboardAvoidingView
            behavior={Platform.OS === 'ios' ? 'padding' : undefined}
            style={{ display: 'flex', flex: 1 }}
            keyboardVerticalOffset={KEYBOARD_VERTICAL_OFFSET}
            // enabled={Platform.OS === 'ios' ? true : false}
          >
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
              <View style={styles.authContainer}>
                <Text style={styles.heading}>Forgot your password?</Text>
                <Text style={styles.description}>
                  Enter your email address to reset your password
                </Text>

                <Text style={styles.label}>
                  Email<Text style={styles.specicalText}>*</Text>
                </Text>
                <Input
                  LeftIcon={<EmailIcon />}
                  placeholder={'Enter your email'}
                  onChangeText={handleChange('email')}
                  value={values.email}
                  error={errors.email ? true : false}
                  autoFocus={true}
                />

                <View style={styles.buttonWraper}>
                  <Button
                    disabled={!values.email || errors.email ? true : false}
                    type="warning"
                    style={{
                      width: '100%',
                      backgroundColor: '#E3253C',
                    }}
                    onPress={() => {
                      handleForgotPassword(values.email)
                    }}
                  >
                    {isSubmit ? <ActivityIndicator color="#fff" /> : 'Submit'}
                  </Button>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </KeyboardAvoidingView>
        )}
      </Formik>

      <Dialog.Container visible={visible}>
        <View style={styles.dialogContainer}>
          <Dialog.Title>Error</Dialog.Title>
          <Dialog.Description>Email is Error !!</Dialog.Description>
          <Dialog.Button
            label="Ok"
            onPress={() => {
              setVisible(false)
            }}
          />
        </View>
      </Dialog.Container>
    </>
  )
}

export default React.memo(ForgotPassword)

const styles = StyleSheet.create({
  //diaglog
  dialogContainer: {
    // flex: 1,
    // backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },

  //
  specicalText: { color: '#E3253C' },
  authContainer: {
    display: 'flex',
    flex: 1,
    paddingHorizontal: 24,
  },
  heading: {
    color: '#323232',
    fontWeight: '700',
    fontSize: 26,
    lineHeight: 36,
  },
  description: {
    fontSize: 16,
    lineHeight: 22,
    color: '#5F5F5F',
  },
  label: {
    fontSize: 16,
    fontWeight: '600',
    paddingTop: 32,
    paddingBottom: 12,
  },
  buttonWraper: {
    position: 'absolute',
    bottom: 0,
    left: 24,
    right: 24,
    marginBottom: 30,
  },
})
