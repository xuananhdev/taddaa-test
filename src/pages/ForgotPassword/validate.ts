// App.js
import * as yup from 'yup'

const loginValidationSchema = yup.object().shape({
  email: yup.string().email(),
})

export default loginValidationSchema
