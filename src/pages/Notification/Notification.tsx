import { Text, View, StyleSheet, TouchableOpacity, FlatList, SafeAreaView } from 'react-native'
import React, { useEffect, useState } from 'react'

import { useDispatch, useSelector } from 'react-redux'
import { ActivityIndicator } from '@ant-design/react-native'
import moment from 'moment'

import { saveNotif, changeIsRead } from '~store/notifSlice'
import { notifListSelector } from '~store/notifSlice/slector'
import { appUserSelector } from '~store/userSlice/selector'

import notifServices from '~/services/axios/notif'

import CloseIcon from '../../access/icons/close.svg'

import type { NotificationType } from '~store/notifSlice/type'

export interface IProps {
  setShowModal: Function
}

const Notification = ({ setShowModal }: IProps) => {
  const dispatch = useDispatch()
  const { employeeId } = useSelector(appUserSelector)

  const [isLoading, setLoading] = useState(false)
  const [isFetchingNextPage, setFetchingNextPage] = useState(false)

  const listNotifSelector = useSelector(notifListSelector)

  useEffect(() => {
    if (employeeId) {
      try {
        async function getListNotif() {
          setLoading(true)
          const resListNotif = await notifServices.getListNotif(employeeId, 0, 20)

          const listNotification = resListNotif?.data?.data?.eOffice_Notification?.items

          dispatch(saveNotif({ listNotification }))
          setLoading(false)
        }
        getListNotif()
      } catch (error) {
        console.log('getListNotif error', error)
        setLoading(false)
      }
    }
  }, [employeeId])

  const _markAsRead = async (item: NotificationType) => {
    try {
      const res = await notifServices.markAsRead(item.contentItemId)

      dispatch(changeIsRead({ ...item, isRead: true }))
    } catch (error) {
      console.log('markAsRead error', error)
    }
  }

  const loadMore = () => {
    async function fetchLoadMore() {
      setFetchingNextPage(true)
      const resListNotif = await notifServices.getListNotif(
        employeeId,
        listNotifSelector.length,
        20,
      )
      const listNotification = resListNotif?.data?.data?.eOffice_Notification?.items
      setFetchingNextPage(false)
      dispatch(saveNotif({ listNotification: [...listNotifSelector, ...listNotification] }))
    }
    fetchLoadMore()
  }

  const renderSpinner = () => {
    return <ActivityIndicator text="Loading..." color={'green'} size="large" />
  }

  return (
    <SafeAreaView style={styles.modalNotifContainer}>
      {isLoading ? (
        <View style={styles.loadingWrapper}>{renderSpinner()}</View>
      ) : (
        <>
          <View style={styles.modalHeaderContainer}>
            <TouchableOpacity
              style={{ marginLeft: 29, padding: 20 }}
              onPress={() => {
                setShowModal(false)
              }}
            >
              <CloseIcon />
            </TouchableOpacity>
            <View style={styles.modalHederTitleWrapper}>
              <Text style={styles.modalHeaderTitle}>Notifications</Text>
            </View>
          </View>

          <FlatList
            data={listNotifSelector}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => (
              <View style={styles.modelContentContainer}>
                <View
                  style={{
                    ...styles.notifDot,
                    backgroundColor: item.isRead ? '#D3D6D9' : '#E3253C',
                  }}
                />
                <View style={styles.notifCardContainer}>
                  <View>
                    <Text style={styles.notifTitle}>{item.title}</Text>
                    <Text style={styles.notifDate}>
                      {moment(item.createdAt).format('HH:mm DD/MM/YYYY')}
                    </Text>
                    <Text style={styles.notifDescription}>{item.description}</Text>
                  </View>

                  {!item.isRead && (
                    <TouchableOpacity onPress={() => _markAsRead(item)} style={{ marginTop: 12 }}>
                      <Text style={styles.notifMark}>Mark as read</Text>
                    </TouchableOpacity>
                  )}
                </View>
              </View>
            )}
            onEndReached={loadMore}
            onEndReachedThreshold={0.3}
            ListFooterComponent={isFetchingNextPage ? renderSpinner : null}
            showsVerticalScrollIndicator={false}
          />
        </>
      )}
    </SafeAreaView>
  )
}

export default React.memo(Notification)

const styles = StyleSheet.create({
  loadingWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  dotNotif: {
    height: 10,
    width: 10,
    borderRadius: 5,
    position: 'absolute',
    right: -5,
    top: -4,
    backgroundColor: '#E3253C',
  },
  // Modal
  modalNotifContainer: {
    display: 'flex',
    flex: 1,
  },
  modalHeaderContainer: {
    height: 107,
    backgroundColor: '#FFFFFF',
    borderBottomColor: '#D3D6D9',
    borderBottomWidth: 1,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  modalHederTitleWrapper: {
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalHeaderTitle: {
    fontSize: 16,
    fontWeight: '600',
    justifyContent: 'center',
    color: '#323232',
    marginRight: 29,
  },

  modelContentContainer: {
    display: 'flex',
    flex: 1,
    paddingHorizontal: 40,
    flexDirection: 'row',
  },
  notifCardContainer: {
    display: 'flex',
    flexDirection: 'column',
    paddingVertical: 24,
    borderBottomWidth: 1,
    borderColor: '#D3D6D9',
  },

  notifDot: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginTop: 30,
    marginRight: 8,
  },
  notifTitle: {
    color: '#323232',
    fontWeight: '600',
    fontSize: 16,
  },
  notifDate: {
    fontSize: 12,
    color: '#5F5F5F',
    marginTop: 8,
    marginBottom: 12,
  },
  notifDescription: {
    fontWeight: '600',
    lineHeight: 24,
    color: '#5F5F5F',
  },
  notifMark: {
    color: '#4C75DF',
    fontSize: 16,
    fontWeight: '600',
    textAlign: 'right',
  },
  //
})
